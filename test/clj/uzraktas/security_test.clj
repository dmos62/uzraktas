(ns uzraktas.security-test
  (:require [clojure.test :refer :all]
            [uzraktas.test-helpers :refer :all]
            [uzraktas.security :as sec]
            [ring.mock.request :as req])
  (:import (com.google.appengine.tools.development.testing
             LocalServiceTestConfig LocalUserServiceTestConfig
             LocalServiceTestHelper)
           (com.google.appengine.api.users
             UserService UserServiceFactory)))


(defn- local-user-service-helper []
  (LocalServiceTestHelper.
    (into-array LocalServiceTestConfig
                [(LocalUserServiceTestConfig.)])))

(defmacro deftest-user [user-state & body]
  (let [ admin (= user-state :admin)
        logged-in (or (= user-state :admin) (= user-state :logged-in))]
  `(deftest-nameless
     (let [helper# (-> (local-user-service-helper)
                       (.setEnvIsAdmin ~admin)
                       (.setEnvIsLoggedIn ~logged-in))]
       (.setUp helper#)
       (try
         ~@body
         (finally (.tearDown helper#)))))))

(def admin-only-app (sec/wrap-admin-only (fn [_] {:status 200 :body "OK"})))

(comment "Testing app responses when user is not logged in OR logged in,
          but not admin OR logged in and admin. Also testing enforcement
          of HTTPS.")

(deftest-user :not-logged-in
  (let [req (req/request :get "some-uri")]
    (is (= (:status (admin-only-app req)) 403)))
  (let [req (assoc (req/request :get "some-uri") :scheme :https)]
    (is (= (:status (admin-only-app req)) 403))))


(deftest-user :logged-in
  (let [req (req/request :get "some-uri")]
    (is (= (:status (admin-only-app req)) 403)))
  (let [req (assoc (req/request :get "some-uri") :scheme :https)]
    (is (= (:status (admin-only-app req)) 403))))

(deftest-user :admin
  (let [req (req/request :get "some-uri")]
    (is (= (:status (admin-only-app req)) 301)))
  (let [req (assoc (req/request :get "some-uri") :scheme :https)]
    (is (= (:status (admin-only-app req)) 200))))
