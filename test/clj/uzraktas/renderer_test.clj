(ns uzraktas.renderer-test
  (:require [uzraktas.renderer :as rndrr] 
            [clojure.test :refer :all]
            [uzraktas.test-helpers :refer :all]))

(deftest-nameless 
  (testing "insert-second-recursively"
    (is (= (rndrr/insert-second-recursively
             '(a    a    (a   (a    a  c)))
             'b
             '[c])
           '(a b (a b) (a b (a b (a b) c)))))))

(deftest-nameless
  (testing "render macro"
    (is (= 
          (rndrr/render [x ["eye" {:a "rose"}]] 
            (#(str (% :a) %2 %3) 
                   x
                   (#(str (% :a) %2) x)))
           "roseeyeroseeye"))))
