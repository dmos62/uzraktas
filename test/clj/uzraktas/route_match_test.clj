(ns uzraktas.route-match-test
  (:require [clojure.test :refer :all]
            [uzraktas.test-helpers :refer :all]
            [uzraktas.route-match :refer [apply-bindings-to-list
                                    match match-against-rules match-uri
                                    variable? ]]))

(def dummy-routing-table
  '(
    ("/" ->              (page home))
    ("/about" ->         (page about))
    ("/post/:title" ->   (post :title))
    ("" ->              (page home))
    ("about" ->         (page about))
    ("post/:title" ->   (post :title))
    ; TODO are relative and root (/) uris distinguished? probably not
    ))

(def match-route (partial match-against-rules dummy-routing-table))

(deftest-nameless
  (testing "pattern match elementary vectors"
    (is (match [1 2] [1 2]))
    (is (= {:a 3} (match [1 2 3] [1 2 :a])))
    (is (= {:b 1, :a 3} (match [1 2 3] [:b 2 :a])))
    (is (nil? (match [1 2 3] [1 2])))
    (is (nil? (match [1 2] [1 2 3])))
    (is (nil? (match [1 2] [1 2 :a])))
    (is (nil? (match [1 2 3 4] [1 2 :a])))
    ))

(deftest-nameless
  (testing "apply bindings to list"
    ; binding map'e keys yra stringai, taiciau outpute variables yra simboliai
    (is (= (apply-bindings-to-list 
             {:a "1" :b "bar"} '(foo :a :b baz)) '(foo "1" "bar" baz)))
    (is (= (apply-bindings-to-list 
             {:title "title"} '(post :title)) '(post "title")))
    )
  (testing "variable? finds variables"
    ;(is (variable? "*a"))
    ;(is (variable? '*a))
    (is (variable? :cat))
    (is (variable? ":cat"))
    (is (not (variable? "dog")))
    (is (not (variable? 'dog)))
    )
  (testing "pattern match uri and route"
    (is (= (match-route "/about")        '(page about)))
    (is (= (match-route "/post/title")   '(post "title")))
    (is (= (match-route "/")             '(page home)))
    (is (= (match-route "about")        '(page about)))
    (is (= (match-route "post/title")   '(post "title")))
    (is (= (match-route "")             '(page home)))
    ;Ilgesnis nei vieno elemento match'as nereikalingas, nes galiu
    ;tiesiog iskviesti 404 handleri kai nera jokio match'o.
    )
  (testing "pattern match uri and extract variables"
    (is (= {} (match-uri "/about" "/about")))
    (is (= nil (match-uri "/about/asd" "/about")))
    (is (= nil (match-uri "/about" "/about/asd")))
    (is (= {:title "xyz"} (match-uri "/post/:title" "/post/xyz")))
    (is (= {:string ":xyz"} (match-uri "/post/:string" "/post/:xyz")))
    )
  (testing "different types of variables"
    (is (= {:*path ["a" "b" "c"]} (match-uri "/app/:*path" "/app/a/b/c")))
    (is (= {:*?path ["a" "b" "c"]} (match-uri "/app/:*?path" "/app/a/b/c")))
    (is (= {:*path ["a"]} (match-uri "/app/:*path" "/app/a")))
    (is (= {:*?path []} (match-uri "/app/:*?path" "/app")))
    (is (= {:?path "a"} (match-uri "/app/:?path" "/app/a")))
    (is (= {:?path nil} (match-uri "/app/:?path" "/app")))
    (is (= nil (match-uri "/app/:*path" "/xyz")))
    )
  (testing "match-uri accepts parsed uris"
    (is (= {:*?x ["xyz"]} (match-uri "/app/:*?x" ["app" "xyz"]))))
  )
