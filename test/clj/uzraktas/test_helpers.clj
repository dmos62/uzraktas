(ns uzraktas.test-helpers
  (:require [clojure.test]))

(defmacro deftest-with-name [name & body]
  `(clojure.test/deftest ~name ~@body))

(defmacro deftest-nameless [& body]
  (let [name (gensym "test")]
  `(deftest-with-name ~name ~@body)))
