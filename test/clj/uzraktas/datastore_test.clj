(ns uzraktas.datastore-test
  (:require [clojure.test :refer :all]
            [uzraktas.test-helpers :refer :all]
            [uzraktas.datastore :as dtstr])
  (:import 
    com.google.appengine.api.datastore.FetchOptions$Builder
    (com.google.appengine.api.datastore 
      DatastoreServiceFactory Query Entity)
    (com.google.appengine.tools.development.testing
      LocalServiceTestConfig LocalDatastoreServiceTestConfig
      LocalServiceTestHelper)
    ))

;; Helpers

(defn local-datastore-helper []
  (LocalServiceTestHelper.
    (into-array LocalServiceTestConfig
                [(LocalDatastoreServiceTestConfig.)])))

(defmacro ds-test [& body]
  `(deftest-nameless
     (let [helper# (local-datastore-helper)]
       (.setUp helper#)
       (try
         ~@body
         (finally (.tearDown helper#))))))

;;;;

(def sample-post-entity
  {:kind "content" :content-type "post"
   :content.body "hello" :date "??undefined??" :urn "hi-you-all"})

(def sample-painting-entity
  {:kind "content" :content-type "painting"
   :content.description "This is good painting" 
   :content.image "!image file!" :urn "hi-you-all"})

(ds-test
  (testing "datastore service interface carries out basic operations"
    (let [ds (DatastoreServiceFactory/getDatastoreService)
          count-yam #(.countEntities 
                       (.prepare ds (Query. "yam")))]
      (is (= 0 (count-yam)))
      (.put ds (Entity. "yam"))
      (.put ds (Entity. "yam"))
      (is (= 2 (count-yam)))
      )))

(defn equal-except-key-id? [a b] (= (dissoc a :key :id) (dissoc b :key :id)))

(deftest-nameless
  (let [ent1 {:a "1" :b "2"}
        ent2 {:a "2" :b "1"}
        ent3 {:a "2" :b "1" :key :x}
        ent4 {:a "1" :b "2" :key :x}]
    (is (not (equal-except-key-id? ent1 ent2)))
    (is (not (equal-except-key-id? ent1 ent3)))
    (is (equal-except-key-id? ent1 ent4))
    (is (equal-except-key-id? ent2 ent3))))

(ds-test
  (testing "entity is persisted and updated"
    (let [;ds  (DatastoreServiceFactory/getDatastoreService)
          ent (dtstr/persist sample-post-entity)
          ent2 (dtstr/persist (assoc ent :a "x"))]
      (is (equal-except-key-id? ent sample-post-entity))
      (is (= ent (dissoc ent2 :a)))
      )))

(ds-test
  (testing "entity deletes"
    (let [ds (DatastoreServiceFactory/getDatastoreService)
          ent (dtstr/persist sample-post-entity)
          count-ents #(.countEntities 
                        (.prepare ds (Query. "content")))]
      (is (= 1 (count-ents)))
      (dtstr/delete (:key ent))
      (is (= 0 (count-ents)))
      )))

(ds-test
  (testing "support for one level deep nested maps"
    (let [sample {:kind "some kind" :nest {"x" 1 "y" 2}}
          ent (dtstr/persist sample)
          sample2 {:kind "some kind" :empty-nest {}}
          ent2 (dtstr/persist sample2)]
      (is (equal-except-key-id? ent sample))
      (is (equal-except-key-id? ent2 sample2))
      )))

(ds-test
  (testing "key name support"
    (let [sample {:kind "kind1" :x "x value" :name "the name"}
          ent (dtstr/persist sample)
          ent2 (dtstr/retrieve (dtstr/create-key "kind1" "the name"))]
      (is (= ent ent2))
      )))
