(ns uzraktas.admin.works-mover-test
  (:refer-clojure :exclude [test])
  (:use [uzraktas.test-utils :only [test]]
        [uzraktas.trace :only [TRACE *trace-explicitly-enabled*]]
        [uzraktas.admin.works-mover
         :only [make-gap-from-right-ix make-gap move-works reweigh]]))

(defn make-works [in]
  (let [maker #(hash-map :uid % :weight % :tag :x)]
    (if (sequential? in)
      (vec (map maker in))
      (vec (map maker (range 1 (inc in)))))))

(defn test-works-mover [f]
  (let [test (partial test f "works-mover")]
    (test
      "move-works"
      "test1"
      (=
       (->>
         (move-works (make-works 6) :x [2 5 1] 3)
         (sort-by :weight) (map :uid))
       [1 2 5 3 4 6])
      "test2"
      (=
       (->>
         (move-works (make-works 5) :x [3 4] 2)
         (sort-by :weight) (map :uid))
       [1 3 4 2 5])
      "test3"
      (=
       (->>
         (move-works (make-works 5) :x [1 2 3 4] 5)
         (sort-by :weight) (map :uid))
       [1 2 3 4 5])
      "test4"
      (=
       (->>
         (move-works (make-works [-102 -299 -200 -199 -101]) :x [-102] -199)
         (sort-by :weight) (map :uid))
       [-299 -200 -102 -199 -101])
      "test5"
      (=
       (->>
         (move-works (make-works 2) :x [2] 1)
         (sort-by :weight) (map :uid))
       [2 1])
      "test6"
      (=
       (->>
         (move-works (make-works 3) :x [1] 3)
         (sort-by :weight) (map :uid))
       [2 1 3])
      )
    (let [weighed1 (reweigh (make-works 3) (make-gap 3 5))]
      (test
        "reweigh"
        "test1"
        (=
         (map :weight weighed1)
         [3 4 5])))
    (test
      "make-gap-from-right-ix"
      "test1"
      (=
       (make-gap-from-right-ix (make-works [1 4]) 1)
       (make-gap 2 3)))
    ) nil)
