(ns uzraktas.test.text-decoration-test
  (:refer-clojure :exclude [test])
  (:use [uzraktas.test-utils :only [test]]
        [uzraktas.trace :only [TRACE *trace-explicitly-enabled*]])
  (:require [uzraktas.text-decoration :as d]))

(def text1
  {:markup
   "#Title telelt eta:d asd: :asd
   ##456
   Tantra is an [ancient](http://ancient.com) yet vibrant spiritual science.
   The _tantric_ approach to life avoids this pitfall.

   Tantra sadhana (spritual practice) reweaves the fabric."
   :html
   "<h1>Title telelt eta:d asd: :asd</h1> <h2>456</h2> <p>Tantra is an <a href=\"http://ancient.com\">ancient</a> yet vibrant spiritual science.</p> <p>The <i>tantric</i> approach to life avoids this pitfall.</p> <p>Tantra sadhana (spritual practice) reweaves the fabric.</p>"
   })

(defn test-text-decoration [f]
  (let [test (partial test f "text-decoration")]
    (test
      "heading_m"
      "test1"
      (= ((d/heading d/html-string-interpreter) "#123")
         ["<h1>123</h1>" nil])
      )
    (test
      "paragraph_m"
      "test1"
      (= (ffirst ((d/paragraph_m d/echo) "123\n\n321")) "123")
      "test2"
      (= (second ((d/paragraph_m d/echo) "123\n\n321")) "321")
      "test3"
      (= (ffirst ((d/paragraph_m d/echo) "123")) "123")
      )
    (test
      "plain_m"
      "test1"
      (= ["abc abc" "_abc _abc"] (d/plain_m "abc abc _abc _abc"))
      "test2"
      (= ["abc abc" "[1bc [abc"] (d/plain_m "abc abc [1bc [abc"))
      )
    (test
      "between_m"
      "test1"
      (= [["123" nil] "321 [123"]
         ((d/between_m "[" "]" d/echo) "[123]321 [123"))
      )
    (test
      "image"
      "test1"
      (= ["<img src=\"srcsrc\" alt=\"altalt\">" " foobar"]
         ((d/image d/html-string-interpreter {:images {"cat" "srcsrc"}})
          "![altalt](cat) foobar")))
     (test
      "list_m"
      "test1"
      (= [["foo" "bar" "baz"] "abc"]
         ((d/list_m #"-\s+" d/echo)
          "- foo\n- bar\n-  baz\nabc")))
    (test
      "decorate"
      "test1"
      (= (d/decorate d/html-string-interpreter "_text_" {})
         "<p><i>text</i></p>")
      "test2"
      (= (d/decorate d/html-string-interpreter (:markup text1) {})
         (:html text1))
      )
    ))
