(ns uzraktas.test-utils
  (:refer-clojure :exclude [test])
  (:require
    [uzraktas.trace :refer [TRACE]]))

(defn render-test-run [[title output]]
  (str title ": " (if output "passed" "FAILED")))

(defn test [f group-title unit-title & tests]
  {:pre [(even? (count tests))]}
  (->>
    (partition-all 2 tests)
    (map render-test-run)
    (map #(str group-title " " unit-title " " %))
    (map f)
    dorun))
