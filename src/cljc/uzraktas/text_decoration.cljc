(ns uzraktas.text-decoration
  (:require [clojure.string :as str]
            [uzraktas.trace :refer [TRACE TRACE-2]]))

;(defn starts-with [prefix s] (.startsWith s prefix))

(defn remove-prefix [prefix s]
  (if (string? prefix)
    (when (.startsWith s prefix) (subs s (count prefix)))
    (when-let [prefix (re-find prefix s)]
      (recur prefix s))))

(defn re-quote [s]
  (let [special (set ".?*+^$[]\\(){}|")
        escfn #(if (special %) (str \\ %) %)]
    (apply str (map escfn s))))

(defn find-and-split [delimitter s]
  (let [[l r] (str/split  s (re-pattern (re-quote delimitter)) 2)]
    (when r [l r])))

;===========================

(defn string_m [s]
  {:pre [(string? s)]}
  (fn [input]
    (when-let [input (remove-prefix s input)]
      [s input])))

(defn seq_m [& els]
  {:pre [(every? fn? els)]}
  (fn [unparsed]
    (loop [el (first els)
           els (rest els)
           parsed-acc []
           unparsed-acc unparsed]
      (if-not el
        [parsed-acc unparsed-acc]
        (when-let [[parsed unparsed] (el unparsed-acc)]
          (recur (first els)
                 (rest els)
                 (conj parsed-acc parsed)
                 unparsed))))))

(defn echo [unparsed] [unparsed nil])

(defn between_m [l r parser]
  {:pre [(string? l) (string? r) (fn? parser)]}
  (fn [unparsed]
    (when-let [unparsed (remove-prefix l unparsed)]
      (when-let [[parsed unparsed] (find-and-split r unparsed)]
        [(parser parsed) unparsed]))))

(defn numerous_m [parser]
  {:pre [(fn? parser)]}
  (fn [unparsed]
    (loop [parsed-acc [] unparsed unparsed]
      (if (empty? unparsed)
        [parsed-acc nil]
        (when-let [[parsed unparsed] (parser (str/triml unparsed))]
          (recur (conj parsed-acc parsed) unparsed)
          )))))

(defn or_m [& parsers]
  {:pre [(every? fn? parsers)]}
  (fn [unparsed]
    (loop [parser (first parsers)
           parsers (rest parsers)]
      (when parser
        (if-let [parse (parser unparsed)]
          parse
          (recur (first parsers) (rest parsers))
          )))))

(defn paragraph_m [parser]
  (fn [unparsed]
    (when-let [[parsed unparsed] (str/split (str/triml unparsed) #"\n+" 2)]
      [(parser parsed) unparsed])))

; (defn word_m [parser]
;   (fn [unparsed]
;     ;(when-let [unparsed (not-empty (str/triml unparsed))]
;     (when unparsed
;       (when-let [[parsed unparsed] (split unparsed "(?<=\\S)\\s+")]
;         [(parser parsed) unparsed]
;         ))))

(defn plain_m [unparsed]
  (when unparsed
    (let [[parsed unparsed]
          #?(
             :clj (str/split unparsed #"\s+(?=[_\[!])" 2)
             :cljs (str/split unparsed #"\s+(?=[_\[!])(.+)"))]
      [parsed unparsed]
      )))

;===========================

(defprotocol IInterpreter
  (top-level [_ col])
  (heading-i [_ heading+level])
  (paragraph-i [_ strings])
  (italic-i [_ strings])
  (link-i [this href+text])
  (image-i [this src+alt-text context])
  (ul-i [this items])
  (ol-i [this items]))

(defn join-strings [strings] (apply str (interpose " " strings)))

(def html-string-interpreter
  (reify IInterpreter
    (top-level [_ ss]
      (join-strings ss))
    (heading-i [_ {:keys [heading-level heading]}]
      (str "<h" heading-level ">" heading "</h" heading-level ">"))
    (paragraph-i [_ strings]
      (str "<p>" (join-strings strings) "</p>"))
    (italic-i [_ strings]
      (str "<i>" (join-strings strings) "</i>"))
    (link-i [_ {:keys [href text]}]
      (str "<a href=\"" href "\">" text "</a>"))
    (image-i [_ {:keys [image-id alt-text]} context]
      (str "<img src=\"" (get-in context [:images image-id]) "\" "
           "alt=\"" alt-text "\">"))
    (ul-i [_ items]
      (str "<ul>"
           (->>
             (map
               #(str "<li>" (if (sequential? %) (join-strings %) %) "</li>")
               items)
             (apply str))
           "</ul>"))
    (ol-i [_ items]
      (str "<ol>"
           (->>
             (map
               #(str "<li>" (if (sequential? %) (join-strings %) %) "</li>")
               items)
             (apply str))
           "</ol>"))
    ))

;===========================

; (defn word [unparsed]
;   (when-let [[parsed unparsed] ((word_m echo) unparsed)]
;     [(first parsed) unparsed]))

(defn italic [ntrprtr parser]
  (fn [unparsed]
    (when-let [[parsed unparsed]
               ((between_m "_" "_" (numerous_m parser)) unparsed)]
      [(italic-i ntrprtr (first parsed)) unparsed]
      )))

(defn paragraph [ntrprtr parser]
  (fn [unparsed]
    (when-let [[parsed unparsed2] ((paragraph_m (numerous_m parser)) unparsed)]
      [(paragraph-i ntrprtr (first parsed)) unparsed2]
      )))

(defn heading [ntrprtr]
  (fn [unparsed]
    (let [unparsed (str/triml unparsed)]
      (when-let [hashes (re-find #"^#+" unparsed)]
        (let [heading-level (count hashes)
              [heading unparsed]
              (str/split unparsed #"\n+" 2)
              heading (subs heading heading-level)]
          [(heading-i ntrprtr {:heading-level heading-level :heading heading})
           unparsed]
          )))))

(defn link [ntrprtr]
  (fn [unparsed]
    (when-let [[parsed unparsed]
               ((seq_m
                  (between_m "[" "]" echo)
                  (between_m "(" ")" echo))
                unparsed)]
      (let [[[text] [href]] parsed]
        [(link-i ntrprtr {:href href :text text}) unparsed]))))

(defn image_m [unparsed]
  (when-let [[parsed unparsed]
             ((seq_m
                (between_m "![" "]" echo)
                (between_m "(" ")" echo))
              unparsed)]
    (let [[[alt-text] [image-id]] parsed]
      [{:alt-text alt-text :image-id image-id} unparsed]
      )))

(defn image [ntrprtr context]
  (fn [unparsed]
    (when-let [[alt-text+image-id unparsed] (image_m unparsed)]
      [(image-i ntrprtr alt-text+image-id context) unparsed])))

(defn list_m [item-prefix-exp parser]
  (fn [unparsed]
    (loop [items []
           unparsed unparsed]
      (if-let [unparsed (when unparsed
                          (remove-prefix item-prefix-exp unparsed))]
        (let [[item unparsed] (str/split (str/triml unparsed) #"\n+" 2)]
          (when-let [parsed (parser item)]
            (recur (conj items parsed) unparsed)))
        (when-not (empty? items)
          [(map first items) unparsed])))))

(defn ul [ntrprtr parser]
  (fn [unparsed]
    (when-let [[parsed unparsed]
               ((list_m #"-\s+" (numerous_m parser)) unparsed)]
      [(ul-i ntrprtr (first parsed)) unparsed]
      )))

(defn ol [ntrprtr parser]
  (fn [unparsed]
    (when-let [[parsed unparsed]
               ((list_m #"\d+\.\s+" (numerous_m parser)) unparsed)]
      [(ol-i ntrprtr parsed) unparsed]
      )))

;===========================

(defn decorate [ntrprtr unparsed context]
  (let [link (link ntrprtr)
        italic (italic ntrprtr (or_m link plain_m))
        image (image ntrprtr context)
        paragraph (paragraph ntrprtr (or_m link italic plain_m))
        ol (ol ntrprtr (or_m link italic plain_m))
        ul (ul ntrprtr (or_m link italic plain_m))]
    (->>
      ((numerous_m
         (or_m
           (heading ntrprtr)
           image
           ol
           ul
           paragraph))
       unparsed)
      first
      (top-level ntrprtr))
    ))
