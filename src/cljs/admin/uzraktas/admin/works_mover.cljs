(ns uzraktas.admin.works-mover
  (:use [uzraktas.trace :only [TRACE-2]]))

(defn- first-index [col pred]
  (first (keep-indexed 
           #(when (pred %2) %1) col)))

(def +default-weight-interval+ 100)

(defn distinguish-tag [ws tag]
  (let [{tagged true other false}
        (group-by #(= (:tag %) tag) ws)]
    (list tagged other)))

(defn distinguish-to-move [ws uids-to-move]
  (let [{to-move true other false}
        (group-by #(some? ((set uids-to-move) (:uid %))) ws)]
    (list to-move other)))

(defn make-gap [l r]
  (when (<= l r) {:left-incl l :right-incl r}))

(defn make-gap-from-right-ix [move-context dest-right-ix]
  {:pre [(vector? move-context)]}
  (let [left-outer (get-in move-context [(dec dest-right-ix) :weight])
        right-outer (get-in move-context [dest-right-ix :weight])
        left-incl (when left-outer (inc left-outer))
        right-incl (dec right-outer)]
    #_(TRACE "move-context" move-context
           "dest-right-ix" dest-right-ix
           "left-outer" left-outer
           "right-outer" right-outer
           "left-incl" left-incl
           "right-incl" right-incl)
    (make-gap left-incl right-incl)))

(defn fill-gap [left right]
  (make-gap (when-not (empty? left)
              (inc (:weight (last left))))
            (when-not (empty? right)
              (dec (:weight (first right))))))

(defn width [gap]
  (let [left-incl (:left-incl gap)
        right-incl (:right-incl gap)]
    (when (and left-incl right-incl)
      (inc (- right-incl left-incl)))))

(defn gap-unbound? [gap]
  (when-not (nil? gap)
    (or (not (:left-incl gap)) (not (:right-incl gap)))))

(defn gap-wide-enough-or-unbound? [gap to-move]
  (if-let [width (width gap)]
    (>= width (count to-move))
    (gap-unbound? gap)))

(defn calculate-interval [gap to-move]
  (if-let [width (width gap)]
    (quot width (count to-move))
    +default-weight-interval+))

(defn reweigh [to-move gap]
  (let [interval (calculate-interval gap to-move)
        left-bound? (some? (:left-incl gap))
        to-move (if-not left-bound? (reverse to-move) to-move)
        op
        (if left-bound?
          #(assoc %2 :weight
                  (+ (dec (:left-incl gap)) (* (inc %1) interval)))
          #(assoc %2 :weight
                  (- (inc (:right-incl gap)) (* (inc %1) interval))))]
    (TRACE-2 "interval" interval
             "left-bound?" left-bound?
             "to-move" to-move)
    (map-indexed op to-move)))

(defn dissect-move-context [move-context dest-right-ix]
  (let [[left right] (split-at dest-right-ix move-context)]
    [(reverse left) right]))

(defn move-works [ws tag uids-to-move dest-right-uid]
  {:pre [(not (some #{dest-right-uid} uids-to-move))]
   :post [(vector? %)]}
  (let [[tagged other-tags] (distinguish-tag ws tag)
        [to-move move-context] (->> (distinguish-to-move ws uids-to-move)
                                    (map #(sort-by :weight %))
                                    (map vec))
        dest-right-ix (first-index
                        move-context
                        #(= dest-right-uid (:uid %)))
        gap (make-gap-from-right-ix move-context dest-right-ix)
        [left right] (split-at dest-right-ix move-context)]
    (TRACE-2 "ws" (map #(select-keys % [:weight :uid]) ws)
           "to-move" (map #(select-keys % [:weight :uid]) to-move)
           "move-context" 
           (map #(select-keys % [:weight :uid]) move-context)
           "dest-right-ix" dest-right-ix
           "gap" gap
           "left" (map #(select-keys % [:weight :uid]) left)
           "right" (map #(select-keys % [:weight :uid]) right))
    (loop [switch true
           to-move to-move
           [left right] [left right]
           gap gap]
      (TRACE-2 "switch" switch
               "to-move" to-move
               "left" left
               "right" right
               "gap" gap)
      (cond
        (gap-wide-enough-or-unbound? gap to-move)
        (-> (reweigh to-move gap)
            (concat left right other-tags)
            vec)
        true
        (let [take-off (if switch (last left) (first right))
              to-move (if switch
                        (concat [take-off] to-move)
                        (concat to-move [take-off]))
              [left right] (if switch 
                             [(drop-last left) right]
                             [left (rest right)])
              gap (fill-gap left right)]
          (recur (not switch) to-move [left right] gap)
          )))))
