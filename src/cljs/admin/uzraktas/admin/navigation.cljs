(ns uzraktas.admin.navigation
  (:require [om.core :as om :include-macros true]
            [uzraktas.trace :refer [TRACE]]
            [uzraktas.route-match :refer [match-uri]]
            [uzraktas.admin.cursors :refer [get-token-info-cursor]]
            [goog.events]
            [goog.history.Html5History]
            [goog.history.EventType]
            [goog.Uri]))

(defn match-token [pattern token]
  (match-uri pattern token))

; ===========================================================================

(defn set-app-token! [path]
  (let [token-info @(get-token-info-cursor)
        token (subs path (count (:prefix token-info)))]
    (om/update! (get-token-info-cursor) :token token)
    ))

; ===========================================================================

(defonce history (doto
                   (goog.history.Html5History.)
                   (.setUseFragment false)
                   (.setEnabled true)
                   (.setPathPrefix "")))

(defn set-token! [token] (.setToken history token))

(defn handle-navigate! [e]
  (when (.-isNavigation e) 
    (set-app-token! (.-token e))))

(goog.events/listen history goog.history.EventType/NAVIGATE handle-navigate!)

(defn navigate-in-app! [e]
  (.preventDefault e)
  (let [path (.getPath (goog.Uri. (.. e -target -href)))]
    (.preventDefault e)
    (set-app-token! path)
    (set-token! path)))

