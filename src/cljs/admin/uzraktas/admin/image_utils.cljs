(ns uzraktas.admin.image-utils
  (:require [om.core :as om :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(extend-type js/FileList
  ISeqable
  (-seq [fl] (map #(.item fl %) (range (.-length fl)))))

(defn get-dataurl+file [file c]
  (let [reader (js/FileReader.)]
    (.readAsDataURL reader file)
    (aset reader "onload" #(put! c [(.-result reader) file]))))

(defn <input-to-dataurl+file-c [owner input-ref]
  (let [input-el (om/get-node owner input-ref)
        files (.-files input-el)
        file-c (to-chan files)
        url-file-c (chan (count files))]
    (go-loop
      []
      (when-let [f (<! file-c)]
        (get-dataurl+file f url-file-c)
        (recur)))
    url-file-c))

(defn put-image!
  [entity-cursor kee data-url file]
  (om/transact!
    entity-cursor
    #(-> % 
         (assoc-in [:images kee] data-url)
         (assoc-in [:image-files kee] file))))
