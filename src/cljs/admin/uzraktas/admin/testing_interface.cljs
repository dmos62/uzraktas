(ns uzraktas.admin.testing-interface
  (:require [om.core :as om :include-macros true]
            [om.dom :as omdom :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async :refer [close! take! chan put! <! >! to-chan]]
            [uzraktas.admin.works-mover-test :refer [test-works-mover]]
            [uzraktas.test.text-decoration-test :refer [test-text-decoration]]
            [uzraktas.trace :refer [TRACE]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

;(enable-console-print!)

(defn get-test-button [output-chan]
  (fn [test-f title]
    (let [put-to-output! #(put! output-chan %)]
      (dom/button
        {:class "pure-button"
         :on-click #(test-f put-to-output!)} title))))

(defn testing-interface [_ owner]
  (reify 
    om/IInitState
    (init-state [_] {:output-chan (chan) :output []})
    om/IWillMount
    (will-mount [_]
      (go-loop [] (when-let [output (<! (om/get-state owner :output-chan))]
                    (om/update-state! owner :output #(conj % output))
                    (recur))))
    om/IRender
    (render [_]
      (let [output (om/get-state owner :output)
            test-button (get-test-button (om/get-state owner :output-chan))]
        (dom/div
          {:id "testing-interface"
           :class "pure-g"}
          (dom/div
            {:class "pure-u-1"}
            (test-button test-works-mover "test-works-mover"))
          (dom/div
            {:class "pure-u-1"}
            (test-button test-text-decoration "test-text-decoration"))
          (dom/div
            {:class "pure-u-1 output"}
            (interpose (dom/br) output)))))))
