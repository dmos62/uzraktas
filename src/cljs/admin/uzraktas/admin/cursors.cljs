(ns uzraktas.admin.cursors
  (:require [om.core :as om :include-macros true]))

(defonce state
  (atom {
         :paintings []
         :ref-paintings []
         :posts []
         :ref-posts []
         :statics []
         :ref-statics []
         :token-info {:prefix "/admin"}
         }))

; Kazkodel reikia naudoti ref-cursor o ne root-cursor
(defn get-main-cursor [] (om/ref-cursor (om/root-cursor state)))

(defn ref-cursor [sub-key]
  (om/ref-cursor (get (get-main-cursor) sub-key)))

(defn get-token-info-cursor [] (ref-cursor :token-info))

(defn get-works-cursor [] (ref-cursor :paintings))

(defn get-ref-works-cursor [] (ref-cursor :ref-paintings))

(defn get-posts-cursor [] (ref-cursor :posts))

(defn get-ref-posts-cursor [] (ref-cursor :ref-posts))

(defn get-statics-cursor [] (ref-cursor :statics)) 

(defn get-ref-statics-cursor [] (ref-cursor :ref-statics)) 
