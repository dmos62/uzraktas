(ns uzraktas.admin.statics-editor
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [uzraktas.trace :refer [TRACE]]
           ;[cljs.core.async
           ; :refer [close! take! chan put! <! >! to-chan]
           ; :as async]
           ;[clojure.string :as str]
            [uzraktas.admin.utils :as utils]
            [uzraktas.admin.cursors :as cursors]
            [uzraktas.admin.navigation
             :refer [navigate-in-app! match-token]]
            [uzraktas.admin.resource-comms
             :refer [map->Resource Editor
                     update-local! persist! persist-changes! insert-new!]]
            [uzraktas.admin.om-dom-interpreter :as om-dom-interpreter]
            [cljs.reader]
            ))

; ===========================================================================

(defn form-new []
  {:uid (utils/get-uid)
   :embedded-edn (pr-str {})})

(def resource (map->Resource {:cursor (cursors/get-statics-cursor)
                              :ref-cursor (cursors/get-ref-statics-cursor)
                              :url "/resource/statics"
                              :make-default-new form-new}))

(def editor (Editor. resource))

; ===========================================================================

(defn static-row [static owner]
  (reify
    om/IRender
    (render [_]
      (TRACE "rendering" static)
      (dom/div 
        (dom/a {:href (str "/admin/statics" "/" (:uid static))
                :on-click navigate-in-app!} "Redaguoti")
        (dom/br)
        (or (:name static) "[be pavadinimo]")))))

(defn new? [reference x]
  (let [x (if (:delete x) x (dissoc x :delete))]
    (not (some #{x} reference))))

(defn statics-list [_ owner]
  (reify
    om/IRender
    (render [_]
      (let [statics (om/observe owner (cursors/get-statics-cursor))
            ref-statics (om/observe owner (cursors/get-ref-statics-cursor))]
        (dom/div
          (dom/ul
            (map
              #(dom/li
                 {:class (when (new? ref-statics %) "changed")}
                 (om/build static-row % {:key :uid}))
              statics)
            )
          (dom/button
            {:class "pure-button"
             :on-click #(insert-new! editor {:name "about"})}
            "Naujas \"apie\" įrašas.")
          )))))

; ===========================================================================

(defn event-target-value [e] (.. e -target -value))

(defn edit-about
  [static owner]
  (reify
    om/IRender
    (render [_]
      (let [embedded-edn (cljs.reader/read-string (:embedded-edn static))
            update-embed!
            (fn [k v]
              (om/transact!
                static :embedded-edn
                #(-> % cljs.reader/read-string (assoc k v) pr-str)))]
        (dom/div
          ;Save
          (dom/button {:class "pure-button pure-button-primary"
                       :on-click #(persist! editor static)}
                      "Išsaugoti")
          ;Delete?
          (dom/button
            {:class "pure-button"
             :on-click (fn [] (om/transact! static :delete #(not %)))}
            (if (:delete static) "Netrinti" "Trinti"))
          (dom/form
            {:class "pure-form"}
            (dom/input {:type "text" :value (:name static) :readOnly true}))
          ;text
          (dom/textarea {:value (:text embedded-edn)
                         :class "long-text-area"
                         :placeholder "Kūnas"
                         :on-change
                         #(update-embed! :text (event-target-value %))})
          (dom/div
            {:class "interpreted"}
            (when (:text embedded-edn)
              (om-dom-interpreter/decorate (:text embedded-edn) static)))
          )))))

; ===========================================================================

(defn get-by-uid
  {:pre [(string? uid)]}
  [uid cursor]
  (some #(when (= uid (:uid %)) %) cursor))

(defn statics-editor [subpath owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (update-local! editor))
    om/IRender
    (render [_]
      (if subpath
        (if-let [static
                 (get-by-uid
                   subpath (om/observe owner (cursors/get-statics-cursor)))]
          (case (:name static)
            "about" (om/build edit-about static))
          (dom/span "Tokio iraso nera (arba jis kraunamas)."))
        (om/build statics-list nil)
        ))))
