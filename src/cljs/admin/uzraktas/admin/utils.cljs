(ns uzraktas.admin.utils
  (:import [goog.ui IdGenerator]))

; (defonce used-uids (atom #{}))

; (defonce uid-pool (atom #{}))

; (defn populate-uid-pool [limit]
;   (loop []
;     (let [cand (rand-int 99999)]
;       (when (and (> limit (count @uid-pool))
;                 (not (contains? @used-uids cand)))
;         (swap! uid-pool conj cand)
;         (recur)))))

; (defn get-uid []
;   (let [uid (first @uid-pool)]
;     (if uid
;       (do (swap! uid-pool disj uid) (swap! used-uids conj uid) uid)
;       (do (populate-uid-pool 15) (recur)))))

(defn get-uid [] (.getNextUniqueId (.getInstance IdGenerator)))
