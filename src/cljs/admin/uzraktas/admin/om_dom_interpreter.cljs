(ns uzraktas.admin.om-dom-interpreter
  (:require
    [uzraktas.text-decoration :as decor]
    [om-tools.dom :as dom :include-macros true]))

(defn merge-consequent-strings [col]
  ; should consequent strings be merged on uzraktas.text-decoration?
  (->>
    (partition-by string? col)
    (map
      #(if (string? (first %))
         (str " " (apply str (interpose " " %))" ")
         %
         ))))

(def om-dom-interpreter
  (reify decor/IInterpreter
    (top-level [_ col] (dom/div col))
    (heading-i [_ {:keys [heading-level heading]}]
      (if (= heading-level 1) (dom/h1 heading) (dom/h2 heading)))
    (paragraph-i [_ strings]
      (dom/p (merge-consequent-strings strings)))
    (italic-i [_ contents]
      (dom/em (merge-consequent-strings contents)))
    (link-i [this {:keys [href text]}]
      (dom/a {:href href} text))
    (image-i [this {:keys [image-id alt-text]} context]
      (dom/img {:src (get-in context [:images image-id])
                :alt alt-text}))
    (ul-i [_ items]
      (dom/ul (map #(dom/li %) items)))
    (ol-i [_ items]
      (dom/ol (map #(dom/li %) items)))
    ))

(def decorate (partial decor/decorate om-dom-interpreter))
