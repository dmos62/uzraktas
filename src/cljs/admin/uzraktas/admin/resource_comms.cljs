(ns uzraktas.admin.resource-comms
  (:require [cljs.reader]
            [clojure.set :as set]
            [om.core :as om :include-macros true]
            [uzraktas.trace :refer [TRACE]]
            [uzraktas.http-client :refer [<req]]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(defprotocol IEditor
  (update-local! [this])
  ;(update-local-by-name! [this neim])
  (persist! [this x])
  (persist-changes! [this])
  (insert-new! [this props]))

(defprotocol IResource
  (form-new [this props]))

(defrecord Resource [cursor ref-cursor url make-default-new]
  IResource
  (form-new [_ props] (merge (make-default-new) props)))

(declare -update-local! -persist-single! -persist-changes! -insert-new!)
         ;-update-local-by-name!)

(deftype Editor [^Resource rsrc]
  IEditor
  (update-local! [_] (-update-local! rsrc))
  ;(update-local-by-name! [_ neim] (-update-local-by-name! rsrc neim))
  (persist! [_ x] (-persist-single! rsrc x))
  (persist-changes! [_] (-persist-changes! rsrc))
  (insert-new! [_ props] (-insert-new! rsrc props)))

; ===========================================================================

(defn use-key-as-uid [p]
  (assoc p :uid (:key p)))

(defn <download [rsrc]
  (go
    (when-let [r (<! (<req :method "GET" :url (:url rsrc)
                           :headers {"Content-Type" "application/edn"}))]
      (mapv use-key-as-uid (vec (cljs.reader/read-string (:body r)))))))

(defn -update-local! [rsrc]
  (go
    (let [ps (<! (<download rsrc))]
      (om/update! (:ref-cursor rsrc) ps)
      (when (empty? @(:cursor rsrc))
        (om/update! (:cursor rsrc) ps)
        ))))

; (defn <download-single [rsrc neim]
;   (go
;     (when-let [r (<! (<req :method "GET"
;                           :url (str (:url rsrc) "/name/" neim)
;                           :headers {"Content-Type" "application/edn"}))]
;       (use-key-as-uid (cljs.reader/read-string (:body r))))))

; (defn -update-local-by-name! [rsrc neim]
;   (go
;     (let [x (<! (<download-single rsrc neim))]
;       (om/update! (:ref-cursor rsrc) ps)
;       (when (empty? @(:cursor rsrc))
;         (om/update! (:cursor rsrc) ps)
;         ))))

; ===========================================================================

(defn remove-vector-item [v i]
  (vec (concat (subvec v 0 i) (subvec v (inc i)))))

(defn first-index [pred col]
  (first (keep-indexed 
           #(when (pred %2) %1) col)))

(defn index-of-uid [v uid]
  (first-index #(= (:uid %) uid) v))

(defn handle-deleted! [rsrc p]
  (let [wc (:cursor rsrc)
        rc (:ref-cursor rsrc)
        r-i (index-of-uid @rc (:uid p))
        w-i (index-of-uid @wc (:uid p))] 
    (om/transact! rc #(remove-vector-item % r-i))
    (om/transact! wc #(remove-vector-item % w-i))))

(defn update-by-index! [c i p]
  (if (= i :append)
    (om/transact! c #(conj % p))
    (om/update! c [i] p)))

(defn handle-updated! [rsrc p]
  (let [wc (:cursor rsrc)
        rc (:ref-cursor rsrc)
        r-i (or (index-of-uid @rc (:uid p)) :append)
        w-i (index-of-uid @wc (:uid p))]
    (update-by-index! rc r-i p)
    (update-by-index! wc w-i p)))

; ===========================================================================

(defn <get-upload-url [cb-url]
  (go (:body (<! (<req :method "POST"
                       :url "/resource/upload-url" :body cb-url)))))

(defn images-url [rsrc ds-key] (str (:url rsrc) "/images/" ds-key))

(defn <upload-images! [rsrc image-files-to-upload ds-key]
  (go
    (when (and (not-empty image-files-to-upload) ds-key)
      (let [form (js/FormData.)
            url (<! (<get-upload-url (images-url rsrc ds-key)))]
        (doseq [[n file] image-files-to-upload]
          (.append form n file))
        (<! (<req :url url :method "POST" :body form))))))

(defn <persist-edn! [rsrc edn]
  (go
    (let [edn (dissoc edn :images :image-files)
          url (:url rsrc)]
      (<! (<req :url url :method "POST" :body (pr-str edn))))))

(defn <delete-images! [rsrc image-ids-to-delete ds-key]
  (go
    (when (and (not-empty image-ids-to-delete) ds-key)
      (let [url (images-url rsrc ds-key)]
        (<! (<req :url url :method "DELETE"
                  :body (pr-str image-ids-to-delete)))))))

(defn read-body [resp] (-> resp :body cljs.reader/read-string))

(defn <chs-to-col [c & cs]
  (go 
    (let [c (if (empty? cs) c (async/merge (cons c cs)))]
      (loop [rs []]
        (if-let [r (<! c)]
          (recur (conj rs r))
          rs
          )))))

(defn <persist! [rsrc x]
  (go
    (let [uid (:uid x)
          image-files-to-upload (:image-files x)
          image-ids-to-delete (keys (filter #(= (val %) :delete) (:images x)))
          responses
          (if-let [ds-key (:key x)]
            (<!
              (<chs-to-col
                (<upload-images! rsrc image-files-to-upload ds-key)
                (<delete-images! rsrc image-ids-to-delete ds-key)
                (<persist-edn! rsrc x)))
            (let [edn-response (<! (<persist-edn! rsrc x))
                  ds-key (:key (read-body edn-response))]
              (->
                (<!
                  (<chs-to-col
                    (<upload-images! rsrc image-files-to-upload ds-key)
                    (<delete-images! rsrc image-ids-to-delete ds-key)))
                (conj edn-response))))]
      (->
        (->>
          (filter some? responses)
          (apply max-key #(.parse js/Date (:date %))))
        read-body
        (assoc :uid uid))
      )))

(defn <delete-entity! [rsrc x]
  (go
    (let [r (<! (<req :method "DELETE"
                      :url (str (:url rsrc) "/" (x :key))))]
      (when (= 200 (:status r))
        {:uid (:uid x) :gone true}
        ))))

(defn -persist-single! [rsrc x]
  (go
    (when-let [r (<! (if (:delete x)
                       (<delete-entity! rsrc x)
                       (<persist! rsrc x)))]
      (if (:gone r)
        (handle-deleted! rsrc r)
        (handle-updated! rsrc r)))))

(defn get-changed 
  ([rsrc]
   (get-changed @(:ref-cursor rsrc) @(:cursor rsrc)))
  ([old nu]
   (seq (set/difference (set nu) (set old)))))

(defn -persist-changes! [rsrc]
  (let [ps (get-changed rsrc)
        cs (map
             #(cond
                ;(:gone %) (go %)
                (:delete %) (<delete-entity! rsrc %)
                true (<persist! rsrc %))
             ps)
        c (async/merge cs)]
    (go-loop
      []
      (when-let [x (<! c)]
        (if (:gone x)
          (handle-deleted! rsrc x)
          (handle-updated! rsrc x))
        (recur)))))

; ===========================================================================

(defn -insert-new! [rsrc props]
  (let [n (form-new rsrc props)]
    (om/transact! (:cursor rsrc) #(conj % n))))
