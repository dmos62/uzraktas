(ns uzraktas.admin.works-editor
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async]
            [clojure.string :as str]
            [goog.events]
            [goog.events.EventType]
            [uzraktas.trace :refer [TRACE]]
            [uzraktas.admin.utils :as utils]
            [uzraktas.admin.cursors :as cursors]
            [uzraktas.admin.navigation :refer [navigate-in-app!]]
            [uzraktas.admin.works-mover :refer [move-works]]
            [uzraktas.admin.resource-comms
             :refer [map->Resource Editor
                     update-local! persist-changes! insert-new!]])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]
                   ))

; ===========================================================================

; #_(goog-extend CustomDragListGroup DragListGroup
;   ([]
;   (goog/base (js* "this")))

  ; Panasu kad visa sita mechanizma pakeicia paprasciausias 
  ; dlg-initialised? testas
; (listenForDragEvents
;   [di]
;   (this-as this ; dabar `this` reiskia ta pati kaip ir javascripte
;     (let [drag-items (.-dragItems_ this)]
;       (when (not-any? (partial = di) drag-items)
;         (goog/base
;           ; reikia sito keisto issireiskimo kad veiktu advanced compiliacija
;           (js* "this") "listenForDragEvents" di)))))

;   (set-custom-insert-fn
;     [f]
;     (this-as this
;       (set! (.-custom-insert-fn this) f)))

;   (insertCurrHoverItem
;     []
;     (this-as this
;       (let [curr-drag-item (.-currDragItem_ this)
;             curr-hover-item (.-currHoverItem_ this)]
;       (.custom-insert-fn this curr-drag-item curr-hover-item)
;       ))))

; #_(goog-extend
;   CustomDragDropItem goog.fx.DragDropItem
;   ([el opt-data]
;   (this-as this (goog/base this el opt-data)))

;   (get-handle-element
;     []
;     (this-as this (.getElementByClass goog.dom "handle" (.-element this))))

;   (getDraggableElement
;     [target]
;     (this-as
;       this
;       (println "getDraggableElement")
;       (let [handle-element (.get-handle-element this)]
;         (when (= target handle-element) handle-element))
;       ))
;   #_(mouseDown_
;     [e]
;     (this-as
;       this
;       (TRACE "mouseDown_" e)
;       (goog.base this "mouseDown_" e)
;     ))
;   )

; ===========================================================================

(defn form-new-painting []
  {:uid (utils/get-uid)
   ;:weight (decide-new-weight) :tag tag :image data-url :file file
   })

; ===========================================================================

(def resource (map->Resource {:cursor (cursors/get-works-cursor)
                              :ref-cursor (cursors/get-ref-works-cursor)
                              :url "/admin/works"
                              :make-default-new form-new-painting}))

(def editor (Editor. resource))

; ===========================================================================

(extend-type js/FileList
  ISeqable
  (-seq [fl] (map #(.item fl %) (range (.-length fl)))))

(defn get-dataurl+file [file c]
  (let [reader (js/FileReader.)]
    (.readAsDataURL reader file)
    (aset reader "onload" #(put! c [(.-result reader) file]))))

(defn decide-new-weight [] 0) ; TODO

(defn append-new-paintings [owner tag]
  (let [input-el (om/get-node owner "new-painting-file-input")
        files (.-files input-el)
        file-c (to-chan files)
        url-file-c (chan (count files))]
    (go-loop
      []
      (when-let [f (<! file-c)]
        (get-dataurl+file f url-file-c)
        (recur)))
    (go-loop
      []
      (when-let [[data-url file] (<! url-file-c)]
        (insert-new! editor {:weight (decide-new-weight)
                             :tag tag :images {"photo" data-url}
                             :image-files {"photo" file}})
        (recur)
        ))))

; ===========================================================================

(defn new? [reference p]
  (let [p (if (:delete p) p (dissoc p :delete))]
    (not (some #{p} reference))))

(defn painting-view [painting owner {:keys [mover]}]
  (reify
    om/IDidMount
    (did-mount [_]
      (let [container (om/get-node owner "painting")
            handle (om/get-node owner "handle")]
        (goog.events/listen
          handle goog.events.EventType/CLICK
          #(do
             (.stopPropagation %)
             (put! mover {:target :handle
                          :component owner
                          :uid (:uid painting)})))
        (goog.events/listen
          container goog.events.EventType/CLICK
          #(put! mover {:target :whole
                        :component owner
                        :uid (:uid painting)}))
        ))
   ;(did-mount [_]
   ;  (let [el (om/get-node owner "painting")
   ;        drag-drop-item (CustomDragDropItem. el (:uid painting))]
   ;    (.addDragDropItem ddg drag-drop-item)))
   ;(did-mount [_]
   ;  (let [el (om/get-node owner "painting")
   ;        dlg-initialised? (.-isInitialized_ dlg)]
   ;    (swap! el-to-id #(assoc % el (:uid painting)))
   ;    (when dlg-initialised?
   ;      (.listenForDragEvents dlg el))))
    om/IRender
    (render [_]
      (let [ref-works (om/observe owner (cursors/get-ref-works-cursor))]
        (dom/div
          {:ref "painting"
           :class (str "painting-view pure-g"
                       (when (new? ref-works painting) " new-painting"))}
          (dom/div
            {:class "pure-u-1-1"}
            (dom/img {:src (get-in painting [:images "photo"])
                      :ref "handle"
                      :class "handle pure-img"}))
          (dom/div
            {:class "pure-u-1-1 pure-form"}
            (dom/textarea
              {:value (:prierasas painting)
               :on-change
               #(om/update! painting :prierasas (.. % -target -value))
               :placeholder "Prierašas"})
            (dom/input
              {:type "text"
               :value (:dimensijos painting)
               :on-change
               #(om/update! painting :dimensijos (.. % -target -value))
               :placeholder "Dimensijos"})
            (dom/button
              {:class "pure-button"
               :on-click
               (fn [] (om/transact! painting :delete #(not %)))}
              (if (:delete painting) "Netrinti" "Trinti"))
            )
        )))))

(defn control-dialogs [tag owner]
  (reify
    om/IRender
    (render [_]
      (dom/div
        {:uid "control-dialogs"}
        (dom/input
          {:style {:display "none"}
           :type "file"
           :uid "new-painting-file-input"
           :ref "new-painting-file-input"
           :multiple true
           :accept ".jpg,.png"
           :on-change #(append-new-paintings owner tag)
           })
        (let [trigger-file-input
              #(.click (om/get-node owner "new-painting-file-input"))]
          (dom/button
            {:class "pure-button"
             :on-click trigger-file-input} "Įkelti naują"))
        (dom/button
          {:class "pure-button"
           :on-click #(persist-changes! editor)} "Išsaugoti pakeitimus")
        ))))

(defn ordered-paintings-view [tag owner]
  (reify
    om/IInitState
    (init-state [_]
      {:mover (chan)
       :works-being-moved []})
    ;(init-state [_]
    ;  (let [ddg (goog.fx.DragDropGroup.)]
    ;    (doto ddg (.addTarget ddg) .init)
    ;    {:ddg ddg}
    ;    ))
    ;(init-state [_]
    ;  (let [dlg (CustomDragListGroup.)
    ;        el-to-id (atom {})]
    ;    (set! (.-updateWhileDragging_ dlg) false)
    ;    (.set-custom-insert-fn dlg
    ;                           #(move-painting tag @el-to-id %1 %2))
    ;    {:dlg dlg
    ;     :el-to-id el-to-id}))
    om/IWillMount
    (will-mount [_]
      nil
     ;(let [mover (om/get-state owner :mover)]
     ;  (go
     ;    (while true
     ;      (let [{:keys [target component uid] :as message} (<! mover)
     ;            works-being-moved (om/get-state owner :works-being-moved)]
     ;        (cond 
     ;          (= target :handle)
     ;          (if-let [i (first-index #(= (:uid %) uid) works-being-moved)]
     ;            (do
     ;              (om/update-state! owner :works-being-moved
     ;                                #(remove-vector-item % i))
     ;              (om/set-state! component :selected false))
     ;            (do
     ;              (om/update-state! owner :works-being-moved
     ;                                #(conj % (dissoc message :target)))
     ;              (om/set-state! component :selected true)))
     ;          (and (not-empty works-being-moved) (= target :whole)
     ;               (not (some #(= (:uid %) uid) works-being-moved)))
     ;          (do
     ;            (om/transact!
     ;              (:cursor resource)
     ;              #(move-works
     ;                 % tag (map :uid works-being-moved) uid))
     ;            (dorun (map
     ;                     #(om/set-state! % :selected false)
     ;                     (map :component works-being-moved)))
     ;            (om/set-state! owner :works-being-moved []))
     ;          )))))
      )
    om/IDidMount
    (did-mount [_]
      nil
      )
    ;(did-mount [_]
    ;  (let [ddg (om/get-state owner :ddg)]
    ;    (goog.events/listen
    ;      ddg
    ;      goog.fx.AbstractDragDrop.EventType/DRAGSTART
    ;      #(TRACE "DRAGSTART" %))))
    ;(did-mount [_]
    ;  (let [paintings-list-el (om/get-node owner "paintings-list")
    ;        dlg (om/get-state owner :dlg)]
    ;    (doto dlg
    ;      (.setFunctionToGetHandleForDragItem
    ;        (fn [el] (.getElementByClass goog.dom "handle" el)))
    ;      (.addDragList paintings-list-el (.-RIGHT_2D DragListDirection))
    ;      .init)))
    om/IRender
    (render [_]
      (let [mover (om/get-state owner :mover)
            ;el-to-id (om/get-state owner :el-to-id)
            all-works (om/observe owner (cursors/get-works-cursor))
            select-sorted-ws (->> (filter #(= tag (:tag %)) all-works)
                                  (sort-by :weight))]
        (dom/div
          (dom/div {:uid "paintings-list" :ref "paintings-list"
                    :class "pure-g"}
                   (map
                     #(dom/div
                        {:class "pure-u-1-1
                                pure-u-sm-1-2
                                pure-u-md-1-3
                                pure-u-lg-1-4
                                pure-u-xl-1-5"}
                        (om/build painting-view %
                                  {:key :uid
                                   :opts {:mover mover
                                          ;:el-to-id el-to-id
                                          }}))
                     select-sorted-ws)
                   ;(om/build-all painting-view select-sorted-ws
                   ;              {:key :uid
                   ;               :opts {:dlg dlg :el-to-id el-to-id}})
                   )
          (om/build control-dialogs
                    tag {:react-key "control-dialogs"})
          )))))

(defn works-editor [tag owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (update-local! editor))
    om/IRender
    (render [_]
      (if tag
        (om/build
          ordered-paintings-view
          tag
          {:react-key (str "ordered-paintings-view-" tag)})
        (dom/ul
          (dom/li
            "Redaguoti pirmaji paveikslu rinkini:"
            (dom/a {:href "/admin/works/1"
                    :on-click navigate-in-app!} "/admin/works/1"))
          (dom/li
            "\" antraji paveikslu rinkini:"
            (dom/a {:href "/admin/works/2"
                    :on-click navigate-in-app!} "/admin/works/2"))
          )
        ))))
