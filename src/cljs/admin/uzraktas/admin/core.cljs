(ns uzraktas.admin.core
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async]
            [clojure.string :as str]
            [uzraktas.trace :refer [TRACE]]
            [uzraktas.http-client :refer [<req]]
            [uzraktas.admin.navigation
             :refer [navigate-in-app! set-app-token! match-token]]
            [uzraktas.admin.cursors :as cursors :refer [get-token-info-cursor]]
            [uzraktas.admin.testing-interface :refer [testing-interface]]
            [uzraktas.admin.works-editor :refer [works-editor]]
            [uzraktas.admin.posts-editor :refer [posts-editor]]
            [uzraktas.admin.statics-editor :refer [statics-editor]]
            [goog.events]
            [goog.events.EventType])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

; ===========================================================================

(enable-console-print!)

(defn resources-index-view [app owner]
  (reify
    om/IRender
    (render [_]
      (dom/ul
        (dom/li "Redaguoti postus:"
                (dom/a {:href "/admin/posts"
                        :on-click navigate-in-app!} "/admin/posts"))
        (dom/li "Redaguoti statikus:"
                (dom/a {:href "/admin/statics"
                        :on-click navigate-in-app!} "/admin/statics"))
       ;(dom/li "Redaguoti darbus:" (dom/a {:href "/admin/works"
       ;                :on-click navigate-in-app!} "/admin/works"))
        (dom/li "Paleisti testus:" (dom/a {:href "/admin/test"
                        :on-click navigate-in-app!} "/admin/test"))
        ))))

; ===========================================================================

(defn breadtrail [app owner] 
  (reify 
    om/IRender
    (render [_]
      (let [token-info (om/observe owner (get-token-info-cursor))
            trail (->>
                    (-> (str (:prefix token-info) (:token token-info))
                        (str/split (re-pattern "/")))
                    (remove empty?))
            crumbs (loop [crumbs [(dom/div "/")]
                          trail-item (first trail)
                          trail (rest trail)
                          href-so-far (str "/" trail-item)]
                     (if-not trail-item
                       crumbs
                       (recur (conj crumbs
                                    (dom/div
                                      (dom/a {:href href-so-far
                                              :on-click navigate-in-app!}
                                             trail-item)
                                      (when-not (empty? trail) "/")))
                              (first trail)
                              (rest trail)
                              (str href-so-far "/" (first trail)))))]
        (dom/div {:id "bread"} "Duonos trupinėliai: " crumbs)))))

(defn admin-content-view [app owner]
  (reify 
    om/IRender
    (render [_]
      (condp match-token (:token (om/observe owner (get-token-info-cursor)))
        ;"works/:?tag" :>> #(om/build works-editor (:?tag %))
        "posts/:?path" :>> #(om/build posts-editor (:?path %))
        "statics/:?path" :>> #(om/build statics-editor (:?path %))
        "test" (om/build testing-interface app)
        "/" (om/build resources-index-view app)
        (om/build (fn [_ _] (om/component (dom/div "404"))) app)
        ))))

(defn root-admin-view [app owner]
  (reify
    om/IRender
    (render [_]
      (dom/div {:id "container" :class "pure-g"}
               (dom/div {:id "top" :class "pure-u-1"}
                        (om/build breadtrail app))
               (dom/div {:id "content" :class "pure-u-1"}
                        (om/build admin-content-view app))))))

; ===========================================================================

(defn main []
  (set-app-token! (.. js/window -location -pathname))
  (om/root
    root-admin-view
    cursors/state
    {:target (.-body js/document)})
  "return what?")

;(defn json-stringify-map [hm] (.stringify js/JSON (clj->js hm)))

(add-watch
  cursors/state nil
  (fn [_ _ o n]
    (let [ommit-meat #(assoc % :text :ommited :description :ommited)
          debug
          (fn [title & xs] 
            (->> (apply str (interpose " " (map pr-str xs)))
                 (.debug js/console title)))]
      (when (not (= (:statics o) (:statics n)))
        ;(debug "Posts change: " (map ommit-meat (:posts n))))
        (debug "Statics change: " (:statics n))
        )
      (when (not (= (:ref-statics o) (:ref-statics n)))
        ;(debug "Ref-posts change: " (map ommit-meat (:ref-posts n))))))
        (debug "Ref-statics change: " (:ref-statics n))
        ))))

(main)
