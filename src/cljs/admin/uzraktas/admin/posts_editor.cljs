(ns uzraktas.admin.posts-editor
  (:require [om.core :as om :include-macros true]
            [om-tools.dom :as dom :include-macros true]
            [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]
             :as async]
            [clojure.string :as str]
            [goog.events]
            [goog.events.EventType]
            ;[markdown.core :refer [md->html]]
            [uzraktas.trace :refer [TRACE]]
            [uzraktas.admin.utils :as utils]
            [uzraktas.admin.cursors :as cursors]
            [uzraktas.admin.navigation
             :refer [navigate-in-app! match-token]]
            [uzraktas.admin.resource-comms
             :refer [map->Resource Editor
                     update-local! persist! persist-changes! insert-new!]]
            [uzraktas.admin.image-utils
             :refer [<input-to-dataurl+file-c put-image!]]
            [uzraktas.admin.om-dom-interpreter :as om-dom-interpreter])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:import [goog.date UtcDateTime] ; http://docs.closure-library.googlecode.com/git/class_goog_date_UtcDateTime.html
           ))

; ===========================================================================

(defn form-new []
  {:uid (utils/get-uid)
   :date (.toIsoString (UtcDateTime.) true)
   :published false})

(def resource (map->Resource {:cursor (cursors/get-posts-cursor)
                              :ref-cursor (cursors/get-ref-posts-cursor)
                              :url "/resource/posts"
                              :make-default-new form-new}))

(def editor (Editor. resource))

; ===========================================================================

(defn post-row [post owner]
  (reify
    om/IRender
    (render [_]
      (dom/div 
        (dom/a {:href (str "/admin/posts" "/" (:uid post))
                :on-click navigate-in-app!} "Redaguoti")
        (dom/br)
        (or (:title post) "[be pavadinimo]")))))

(defn new? [reference x]
  (let [x (if (:delete x) x (dissoc x :delete))]
    (not (some #{x} reference))))

(defn ordered-posts [_ owner]
  (reify
    om/IRender
    (render [_]
      (let [posts (->> (om/observe owner (cursors/get-posts-cursor))
                       (sort-by :date))
            ref-posts (om/observe owner (cursors/get-ref-posts-cursor))]
        (dom/div
          (dom/div {:uid "posts-list" :ref "posts-list"
                    :class "pure-g"}
                   (map
                     #(dom/div
                        {:class
                         (str "pure-u-1-1
                              pure-u-sm-1-2
                              pure-u-md-1-3
                              pure-u-lg-1-4
                              pure-u-xl-1-5"
                              (when (new? ref-posts %) " changed")
                              )}
                        (om/build post-row % {:key :uid}))
                     posts))
          (dom/button
            {:class "pure-button"
             :on-click #(insert-new! editor nil)}
            "Naujas irasas.")
          )))))

(defn event-target-value [e] (.. e -target -value))

(def animals
  ["seal" "frog" "bear" "fox" "chameleon" "goat" "steer" "bison"
   "jackal" "turtle" "pig" "hog" "gnu" "marten" "hare" "mustang" "panda"
   "crocodile" "lynx" "koala" "wildcat" "alpaca" "penguin" "ocelot" "wolf"])

(def adjectives
  ["mountain" "magic" "prairee" "asian" "bengal" "black" "silver" "diamond"
   "blue" "green" "indo" "sea" "red" "plains" "tree" "spotted" "striped"
   "arabian" "sand" "happy" "dancing"])

(defn random-animal-not-in [used]
  (loop [animal (str/capitalize (rand-nth animals))]
    (let [adj (str/capitalize (rand-nth adjectives))
          animal (str adj animal)]
      (if (some #{animal} used)
        (recur animal)
        animal))))

(defn set-header-image [post owner]
  (let [url-file-c (<input-to-dataurl+file-c owner "header-image-file-input")]
    (go
      []
      (when-let [[data-url file] (<! url-file-c)]
        (put-image! post "header-image" data-url file)
        ))))

(defn append-images [post owner]
  (let [url-file-c (<input-to-dataurl+file-c owner "new-images-file-input")]
    (go-loop
      [used-animals (keys (:images post))]
      (when-let [[data-url file] (<! url-file-c)]
        (let [animal (random-animal-not-in used-animals)]
          (put-image! post animal data-url file)
          (recur (conj used-animals animal))
        )))))

(defn single-post
  [post owner]
  (reify
    om/IRender
    (render [_]
      (dom/div
        ;Save
        (dom/button {:class "pure-button pure-button-primary"
                     :on-click #(persist! editor post)}
                    "Issaugoti")
        ;Delete?
        (dom/button
          {:class "pure-button"
           :on-click (fn [] (om/transact! post :delete #(not %)))}
          (if (:delete post) "Netrinti" "Trinti"))
        ;Upload Header Image
        (dom/input
          {:style {:display "none"}
           :type "file"
           ;:uid "new-images-file-input"
           :ref "header-image-file-input"
           :accept ".jpg,.png"
           :on-change #(set-header-image post owner)
           })
        (let [trigger-file-input
              #(.click (om/get-node owner "header-image-file-input"))]
          (dom/button
            {:class "pure-button"
             :on-click trigger-file-input} "Įkelti header atvaizdą"))
        ;Upload Image
        (dom/input
          {:style {:display "none"}
           :type "file"
           ;:uid "new-images-file-input"
           :ref "new-images-file-input"
           :multiple true
           :accept ".jpg,.png"
           :on-change #(append-images post owner)
           })
        (let [trigger-file-input
              #(.click (om/get-node owner "new-images-file-input"))]
          (dom/button
            {:class "pure-button"
             :on-click trigger-file-input} "Įkelti atvaizdą"))
        (dom/form
          {:class "pure-form"}
          ;Published?
          (dom/label
            {:for "published_q" :class "pure-checkbox"}
            (dom/input {:type "checkbox"
                        :id "published_q"
                        :checked (:published post)
                        :on-click
                        ;(fn [] (om/transact! post :published #(not %)))
                        #(om/transact! post :published not)
                        })
            "Skleidžiamas?")
          ;canonical urn
          (dom/input {:type "text"
                      :value (:canonical-urn post)
                      :placeholder "Kanoninis-urn"
                      :on-change
                      #(om/update! post :canonical-urn (event-target-value %))
                      :class "pure-input-1"})
          ;title
          (dom/input {:type "text"
                      :value (:title post)
                      :placeholder "#Teksto pavadinimas"
                      :on-change
                      #(om/update! post :title (event-target-value %))
                      :class "pure-input-1"})
          ;date
          (dom/input {:type "text" :value (:date post) :readOnly true})
          )
        (dom/div
          {:id "images"} 
          (map
            #(dom/div (first %) (dom/img {:src (second %)}))
            (:images post))
          (dom/div {:style {:clear "both"}}))
        ;description
        (dom/textarea {:value (:description post)
                       :class "short-text-area"
                       :placeholder "Aprašymas"
                       :on-change
                       #(om/update! post :description (event-target-value %))})
        ;text
        (dom/textarea {:value (:text post)
                       :class "long-text-area"
                       :placeholder "Teksto kūnas"
                       :on-change
                       #(om/update! post :text (event-target-value %))})
        (dom/div
          {:class "interpreted"}
          ;header image
          (dom/img {:class "header-image pure-img"
                    :src (get-in post [:images "header-image"])})
          (dom/h1 (:title post))
          (dom/article
            (when (:text post)
              (om-dom-interpreter/decorate (:text post) post)
              )))))))

(defn get-by-uid
  {:pre [(string? uid)]}
  [uid cursor]
  (some #(when (= uid (:uid %)) %) cursor))

(defn posts-editor [subpath owner]
  (reify
    om/IWillMount
    (will-mount [_]
      (update-local! editor))
    om/IRender
    (render [_]
      (if subpath
        (if-let [post (get-by-uid
                        subpath (om/observe owner (cursors/get-posts-cursor)))]
          (om/build single-post post)
          (dom/span "Tokio iraso nera (arba jis kraunamas)."))
        (om/build ordered-posts nil)
        ))))
