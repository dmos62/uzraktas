(ns uzraktas.http-client
  (:require [cljs.core.async
             :refer [close! take! chan put! <! >! to-chan]]
            [goog.net.XhrIo :as XhrIo]))

(defn <req [& {:keys [url method body headers]}]
  "XhrIo wrap."
  (let [c (chan)
        headers (apply js-obj (reduce concat (seq headers)))
        cb (fn [e] (let [xhrio (.-target e)
                         resp {:body (.getResponseText xhrio)
                               :status (.getStatus xhrio)
                               :date (.getResponseHeader xhrio "date")}]
                    ;(go (>! c resp)
                    ;    (close! c))
                     (put! c resp)
                     (close! c)))]
    (do (XhrIo/send url cb method body headers) c)))

