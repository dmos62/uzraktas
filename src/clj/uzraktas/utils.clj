(ns uzraktas.utils)

(defn- force-eval [x] (if (sequential? x) (dorun x) x))

(defn pr-str-safe [x] (do (force-eval x) (pr-str x)))
