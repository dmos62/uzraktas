(ns uzraktas.admin-if)

(comment "Helper funkcijos renderinimo funkcijom ir ring handleriams.")

(comment
  (defn- get-editor [ent]
    "Returns editor for entity's content type, or nil if there isn't one."
    nil)

  (defn- entity-with-editor [ent]
    "Returns [entity editor], or just entity if there is no editor for it."
    (if-let [editor (get-editor ent)]
      [ent editor] 
      ent
      ))

  (defn- get-relevant-entities []
    "Returns entities which should be displayed when listing them to admin."
    nil)

  (defn- entity-listing []
    "Returns sequence of entity OR [entity editor] if has editor."
    (let [entities (get-relevant-entities)]
      (map entity-with-editor entities)))



  (defn label+input [label-text form-key]
    (label {"for" form-key} label-text)
    (input {"type" "text", "id" form-key, "name" form-key})
    )

  (defn label+textarea [label-text input-key])

  (defn html-form []
    (form {"action" post-uri, "method" "post"}
      (div
        (label+input "Title" "title"))
      (div
        (label {"for" "body", "id" "body"} "Body")
        (textarea))
      (div
        (button {"type" "submit"}))))
)
