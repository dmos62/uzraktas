(ns uzraktas.security
  (:import (com.google.appengine.api.users
             User UserService UserServiceFactory))
  (:require [ring.middleware.ssl :as rssl]
            [ring.util.response :as rresp]))

#_(defn wrap-require-login
  [application]
  (let [user-service (UserServiceFactory/getUserService)]
    (fn [req]
      (if (.isUserLoggedIn user-service)
        (application req)
        (redirect (.createLoginURL user-service (:uri req)))
        ))))

(defn- wrap-hsts-3-months [app]
  (rssl/wrap-hsts app :max-age 7889230 :include-subdomains? false))

(defn- wrap-enforce-https
  "When HTTPS, implies HSTS."
  [app]
  (let [app (wrap-hsts-3-months app)]
    (rssl/wrap-ssl-redirect app)))

(defn unauthorized [_]
  {:status 403 :body "403: Who are you?"})


(defn wrap-admin-only [app]
  "When admin, implies HTTPS enforcement."
  (let [user-service (UserServiceFactory/getUserService)]
    (fn [req]
      (if (and (.isUserLoggedIn user-service) (.isUserAdmin user-service))
        ((wrap-enforce-https app) req)
        (unauthorized req)))))
