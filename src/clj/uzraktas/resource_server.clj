(ns uzraktas.resource-server
  (:require
    [clojure.edn :as edn]
    [clojure.set :as set]
    [uzraktas.trace :refer [TRACE]]
    [clojure.tools.logging :as log]
    [clojure.algo.generic.functor :refer [fmap]]
    [ring.util.response :refer [response]]
    [ring.util.request :as request]
    [uzraktas.images :refer [get-serving-url]]
    [uzraktas.multipart-parse :refer [wrap-multipart-params]]
    [uzraktas.datastore :as ds]
    [uzraktas.blobstore :as bs]
    [uzraktas.utils :refer [pr-str-safe]])
  (:import
    (com.google.appengine.api.datastore Query Query$SortDirection)))

(defprotocol IResourceServer
  (post-edn [_ req])
  (post-images [_ req ds-key])
  (delete-entity [_ ds-key])
  (delete-images [_ req ds-key])
  (make-query [_])
  (launch-query [_ query] [_ query opts])
  (get-by-name [_ neim])
  (get-by-id [_ id])
  (get-all [_] [_ query-former]))

(defn from-app [ent kind fields]
  (let [images (when-let [k (:key ent)] (:images (ds/retrieve k)))]
    (-> (select-keys ent (conj fields :key :name))
        (assoc :kind kind
               :images images))))

(defn from-ds [ent fields]
  (-> (select-keys ent (conj fields :key :images :name :id))
      (update :images #(fmap get-serving-url %))))

(defn delete-image-blobs! [current-images image-keys-to-delete]
  "Returns updated images."
  (->
    (select-keys current-images image-keys-to-delete)
    vals
    bs/delete-blobs!)
  (apply dissoc current-images image-keys-to-delete))

(defn post-edn-handler [kind fields]
  (fn [req]
    (let [wanted-ent (edn/read-string (request/body-string req))]
      (->
        (from-app wanted-ent kind fields)
        ds/persist
        (from-ds fields)
        pr-str-safe
        response))))

;(defn regulate-images! [uploaded-blobs wanted-ent]
; (let [current-ent (when-let [k (:key wanted-ent)] (ds/retrieve k))
;       current-state (set (keys (:images current-ent)))
;       wanted-state (set (keys (:images wanted-ent)))
;       to-be-deleted (-> (set/difference current-state wanted-state)
;                         (set/union
;                           (set/intersection
;                             current-state
;                             (set (keys uploaded-blobs)))))]
;   (->
;     (select-keys (:images current-ent) to-be-deleted)
;     vals
;     bs/delete-blobs!)
;   (->
;     (apply dissoc (:images current-ent) to-be-deleted)
;     (merge uploaded-blobs))))

(defn post-images-handler [fields ds-key]
  (fn [req]
    (log/debug "POST-IMAGES-HANDLER")
    (log/debug "req" req)
    (let [uploaded-images (bs/get-uploaded-blobs req)
          current-ent (log/spy (ds/retrieve ds-key))
          current-images (:images current-ent)
          image-keys-to-delete
          (set/intersection
            (set (keys current-images))
            (set (keys uploaded-images)))
          wanted-images
          (->
            (delete-image-blobs! current-images image-keys-to-delete)
            (merge uploaded-images))]
      (->
        (assoc current-ent :images wanted-images)
        ds/persist
        (from-ds fields)
        pr-str-safe
        response))))

(defn delete-images-handler [fields ds-key]
  (fn [req]
    (let [image-keys-to-delete (edn/read-string (request/body-string req))
          current-ent (ds/retrieve ds-key)
          current-images (:images current-ent)
          lessened-images
          (delete-image-blobs! current-images image-keys-to-delete)]
      (->
        (assoc current-ent :images lessened-images)
        ds/persist
        (from-ds fields)
        pr-str-safe
        response))))

(deftype ResourceServer [kind fields]
  IResourceServer
  (post-edn [_ req] ((post-edn-handler kind fields) req))
  (post-images [_ req ds-key] ((post-images-handler fields ds-key) req))
  (delete-entity [_ ds-key]
    (let [blob-keys (-> (ds/retrieve ds-key) :images vals)]
      (bs/delete-blobs! blob-keys)
      (ds/delete ds-key)
      {:status 200}))
  (delete-images [_ req ds-key] ((delete-images-handler fields ds-key) req))
  (make-query [_] (Query. kind))
  (launch-query [this query]
    (launch-query this query {}))
  (launch-query [_ query opts]
    (map #(from-ds % fields) (ds/launch query opts)))
  (get-by-name [_ neim]
    (when-let [ent (ds/retrieve (ds/create-key-with-name kind neim))]
      (from-ds ent fields)))
  (get-by-id [_ id]
    (when-let [ent (ds/retrieve (ds/create-key-with-id kind id))]
      (from-ds ent fields)))
  (get-all [this] (get-all this identity))
  (get-all [this query-former]
    (let [query (make-query this)]
      (query-former query)
      (launch-query this query))))
