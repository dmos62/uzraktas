(ns uzraktas.blobstore
  (:require
    [clojure.algo.generic.functor :refer [fmap]]
    [clojure.stacktrace :as st]
    [clojure.tools.logging :as log])
  (:import
    (com.google.appengine.api.blobstore BlobstoreServiceFactory
                                        BlobstoreFailureException)))

(defn get-blobstore-service [] (BlobstoreServiceFactory/getBlobstoreService))

(defn get-blob-upload-url [cb-url]
  (.createUploadUrl (get-blobstore-service) cb-url))

(defn get-uploaded-blobs [req]
  (let [blobstore-service (get-blobstore-service)]
    ; NOTE .getUploads duoda HashMap<String, ArrayList<BlobKey>>
    (->>
      (try
        (.getUploads blobstore-service (:servlet-request req))
        ; IllegalStateException is thrown if not in blob upload cb context
        (catch java.lang.IllegalStateException e {}))
      (into {})
      (fmap first)
      )))

(defn delete-blobs!
  [blob-keys]
  (when-not (empty? blob-keys)
    (try
      (.delete (get-blobstore-service) (into-array blob-keys))
      (catch BlobstoreFailureException e
        (log/error "delete-blobs! failed, blobs not deleted.\n")
        ))))
