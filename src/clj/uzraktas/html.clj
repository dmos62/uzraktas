(ns uzraktas.html
  (:refer-clojure :exclude [meta time]))

(defn- key-to-string [k]
  (cond
    (keyword? k) (name k)
    (string? k) k
    true (throw (Exception. "Neither :keyword, nor \"string\"."))))

(defn- key-val-to-html-attr [[k v]]
  (str
    (key-to-string k) "=" "\"" v "\""))

(defn- map-to-html-attrs [attr-map]
  (->>
    (seq attr-map)
    (map key-val-to-html-attr)
    (interpose " ")
    (apply str)))

(defn create-html-fn [tag-name]
  "Forms a function that generates html.
   Example usage of generated fn:
      (div {:id some-id} actual-html-as-string)
   or
      (div actual-html-as-string)"
  `(defn ~tag-name [& args#]
     (let [first-arg# (first args#)
           attr-map# (if (map? first-arg#) first-arg#)
           html-attrs# (if-not (empty? attr-map#)
                        (str " " (map-to-html-attrs attr-map#)))
           elements# (if attr-map# (rest args#) args#)]
       (str
         "<" ~(name tag-name) html-attrs# ">"
         (apply str elements#)
         "</" ~(name tag-name) ">"
         ))))

(defmacro form-html-fns
  [names]
  `(do ~@(map create-html-fn names)))

(form-html-fns [html head meta script link title
                body div ul ol li p a img time span
                header nav main section article aside address footer
                h1 h2 h3])

(defn css [href]
  (str
    "<link href=\"" href "\" "
    "rel=\"stylesheet\" type=\"text/css\" media=\"all\">"))

(comment "Functions above will be used inside a wrapping function
          that will conform to uzraktas.renderer/render syntax of 
          (f context-map). These low-level constructors do not need
          context information. Context map will be passed to the
          next level of abstraction.")
