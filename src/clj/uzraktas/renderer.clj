(ns uzraktas.renderer)

#_(
  "Render elements of html documents in order of dependence:
   the content is rendered first, then supporting elements 
   that depend on content, and, finally, the enclosing elements."

  "Find the element whose dependencies are satisfied, render it,
   repeat passing on context map. Dependecies have two forms:
   those passed as arguments, and ..."

  "Have the template macro pass `render` and `context` as the two 
   first arguments to every rendering function it calls. Also pass
   the arguments provided in the template as the third, fourth etc arguments."

  "Element which is supposed to be rendered first is specified by
   its symbol (e.g.'post). It is then rendered (called), populating 
   the render map and the context.
   Next the template is walked, rendering satisfied elements, when 
   the already rendered/called elements are encountered, they are 
   recognized to already be in the render map and are skipped."

  "Macro can be easily changed to first render a sequence of elements,
   in specified order, instead of just one element."

  "Template is not walked. It is read by the clojure reader, in that 
   macro produces proper clojure code. Thus arguments are intuitively 
   rendered at the right time, hence no need for custom walker logic."

  "Render map is not neccessary if using native reader. Its only purpose
   was to guide a custom walker."
  )


(defn insert-second-recursively [input the-insert exceptions]
  (cond
    (list? input)
      (->> 
        (map #(insert-second-recursively % the-insert exceptions) (rest input))
        (cons the-insert)
        (cons (first input)))
    (some (partial = input) exceptions) input
    true (list input the-insert)))


(defn get-context [render-obj] (first render-obj))
(defn get-output [render-obj] (second render-obj))

(defmacro render [[initial-bind initial-val] input]
  #_(;"below follows usage example:"
      (render [the-post (something-that-returns-a-render-object)]
        ;"above line could have been equally written:"
        ;render [the-post ["actual-html" {:map-containing :context-information}]
        (html
          head
          (body
            header
            (cont the-post)
            footer))))
  "Takes a bind for initial and the initial itself. The initial is
   [output, context]. Second argument is called input. _Every_ element in
   input is treated as a function call, and is passed the initial's context
   as first argument. Only exception is if initial-bind is found in input,
   it is replaced with initial's output."
  (let [context-symbol (gensym "context-symbol")]
    `(let [initial-render-obj#      ~initial-val
           initial-output#          (get-output initial-render-obj#)
           ~context-symbol          (get-context initial-render-obj#)
           ~initial-bind            initial-output#]
       ~(insert-second-recursively input context-symbol [initial-bind])
       )))

#_(
   (defn body
     [c & elements]
     (str "<body>" (reduce str elements) "</body>"))

   (defn head
     [c]
     (str "<head><title>" (c :title) "</title></head>"))

   (defn post
     [c entity]
     [(interpret-custom-md (entity :body) c)
      (-> c 
          (assoc :title (entity :title))
          (assoc :date  (entity :date)))])
   )
