(ns uzraktas.images
  (:import (com.google.appengine.api.images
            ImagesServiceFactory ServingUrlOptions$Builder)
           (com.google.appengine.api.blobstore BlobKey)))


(defn get-serving-url
  [^:BlobKey blob-key]
  (let [images-service (ImagesServiceFactory/getImagesService)
        serving-url-options (->
                              (ServingUrlOptions$Builder/withBlobKey blob-key)
                              (.secureUrl true))]
    (.getServingUrl images-service serving-url-options)))
