(ns uzraktas.datastore
  (:require [uzraktas.trace :refer [TRACE]])
  (:import (com.google.appengine.api.datastore
             DatastoreServiceFactory Entity EmbeddedEntity
             EntityNotFoundException
             Text
             Key Query KeyFactory
             Query$FilterPredicate Query$FilterOperator
             Query$CompositeFilterOperator
             FetchOptions$Builder
             )))

; (ns uzraktas.content
;   (:import java.util.Date)
;   (:import (com.google.appengine.api.datastore
;             Entity Query
;             Query$FilterPredicate Query$FilterOperator
;             Query$CompositeFilterOperator))
;   (:require [uzraktas.datastore :as ds]))

; (defn equality-filter
;   [property value]
;   (Query$FilterPredicate.
;     property Query$FilterOperator/EQUAL value))

; (defn compose-filters
;   [& filters]
;   (Query$CompositeFilterOperator/and filters))

; #_(defn get-content
;   [content-type urn]
;   (let [content-type-filter (equality-filter "content-type" content-type)
;         urn-filter          (equality-filter "urn" urn)
;         composite-filter    (compose-filters content-type-filter urn-filter)
;         query               (.setFilter (Query. "content") composite-filter)]
;     (ds/launch query)))

; #_(defn get-post-entity [urn] (first (get-content "post" urn)))

(defn create-key-with-id [kind id]
  (if (string? id)
    (recur kind (Long/parseLong id))
    (KeyFactory/createKey kind id)))

(defn create-key-with-name [kind neim]
  (KeyFactory/createKey kind neim))

; (defmulti create-key
;   (fn [kind identifier] (class identifier)))

; (defmethod create-key Long
;   [kind ^Long num-id]
;   (KeyFactory/createKey kind num-id))

; (defmethod create-key String
;   [kind ^String neim]
;   (KeyFactory/createKey kind neim))

(defn embedded-to-map
  "Embedded maps' keys are _strings_."
  [emb]
  (reduce (fn [acc [k v]] (assoc acc k v))
          {}
          (.entrySet (.getProperties emb))))

(defn entity-to-map
  "First level maps' keys are _keywords_."
  [entity]
  (reduce (fn [acc [k v]]
            (assoc acc
                   (keyword k)
                   (cond
                     (instance? EmbeddedEntity v) (embedded-to-map v)
                     (instance? Text v) (.getValue v)
                     true v)))
          (let [ds-key (.getKey entity)
                meta-props
                {:kind (.getKind entity)
                 :key (KeyFactory/keyToString ds-key)}]
            (if-let [neim (.getName ds-key)]
              ; assoc name if it has one, otherwise id
              (assoc meta-props :name neim)
              (assoc meta-props :id (.getId ds-key))))
          (.entrySet (.getProperties entity))))

(defn map-to-embedded [hm]
  {:pre [(every? string? (keys hm))]}
  (let [emb (EmbeddedEntity.)]
    (doseq [[k v] hm]
      (.setProperty emb k v))
    emb))

(defn map-to-entity [hm]
  (let [{ds-key :key kind :kind neim :name id :id} hm
        entity (cond
                 neim (Entity. (create-key-with-name kind neim))
                 id (Entity. (create-key-with-id kind id))
                 ds-key (if (string? ds-key)
                          (Entity. (KeyFactory/stringToKey ds-key))
                          (Entity. ds-key))
                 kind (Entity. kind)
                 true (throw))
        props (dissoc hm :kind :key :name :id)]
    (doseq [[k v] props]
      (->>
        (cond (map? v) (map-to-embedded v)
              (and (string? v) (> (count v) 100)) (Text. v)
              true v)
        (.setProperty entity (name k))))
    entity))

(defn persist
  [item]
  (let [entity (map-to-entity item)]
    (.put (DatastoreServiceFactory/getDatastoreService) entity)
    (entity-to-map entity)))

(defn retrieve
  [kee]
  (if (string? kee)
    (recur (KeyFactory/stringToKey kee))
    (try
      (entity-to-map (.get (DatastoreServiceFactory/getDatastoreService) kee))
      (catch EntityNotFoundException e nil))))

(defn launch
  [#^Query query & [opts]]
  (let [ds (DatastoreServiceFactory/getDatastoreService)
        fetch-options (cond
                        (and (:offset opts) (:limit opts))
                        (->
                          (. FetchOptions$Builder withOffset (:offset opts))
                          (. limit (:limit opts)))
                        (:offset opts)
                        (. FetchOptions$Builder withOffset (:offset opts))
                        (:limit opts)
                        (. FetchOptions$Builder withLimit (:limit opts)))
        results (if fetch-options
                  (.asIterable (.prepare ds query) fetch-options)
                  (.asIterable (.prepare ds query)))]
    (map entity-to-map results)))

(defn delete [kee]
  {:pre [(string? kee)]}
  (->> (KeyFactory/stringToKey kee)
       list into-array
       (.delete (DatastoreServiceFactory/getDatastoreService))))

(defn equality-filter
  [property value]
  (Query$FilterPredicate.
    property Query$FilterOperator/EQUAL value))

(defn compose-filters-and
  [& filters]
  {:pre [(>= (count filters) 2)]}
  (Query$CompositeFilterOperator/and filters))
