(ns uzraktas.core
  (:require
    ;[clojure.edn :as edn]
    ;[clojure.set :as set]
    ;[clojure.algo.generic.functor :refer [fmap]]
    [clojure.tools.logging :as log]
    [ring.util.response :refer [response]]
    [ring.util.request :as request]
    [uzraktas.html :as html]
    [uzraktas.route-match :refer [match-uri]]
    [uzraktas.renderer :refer [render]]
    [uzraktas.datastore :as ds]
    [uzraktas.security :refer [wrap-admin-only]]
    [uzraktas.images :refer [get-serving-url]]
    [uzraktas.blobstore :as bs]
    [uzraktas.multipart-parse :refer [wrap-multipart-params]]
    [uzraktas.resource-server :refer [->ResourceServer
                                      post-edn post-images
                                      delete-entity delete-images
                                      make-query launch-query
                                      get-all get-by-name get-by-id]]
    [uzraktas.text-decoration :as dec]
    [uzraktas.trace :refer [TRACE]]
    [uzraktas.utils :refer [pr-str-safe]]
    [fipp.edn :refer [pprint]])
  (:import
    (com.google.appengine.api.datastore
      Query Query$SortDirection)
    (com.google.appengine.api.users UserServiceFactory)
    (java.util Calendar)))

; ; ; ; ; ; ; ; ; ; ; ;

#_(def navigation
  (html/ul
    (html/li
      (html/a {:href "#"} "Darbai")
      (html/ul (html/li (html/a {:href "#"} "1"))
        (html/li (html/a {:href "#"} "2"))
        (html/li (html/a {:href "#"} "3"))))
    (html/li (html/a {:href "#"} "Apie"))
    (html/li (html/a {:href "#"} "Kontaktai"))))

(defn decorate [text & [context]]
  "Decorate custom markup with html."
  (dec/decorate dec/html-string-interpreter text (or context {})))

(defn document [c body]
  (html/html
    (html/head
      (html/title (:title c))
      ;(html/css "http://yui.yahooapis.com/combo?pure/0.6.0/pure-min.css&pure/0.6.0/grids-responsive-min.css")
      (html/css "/css/grids-responsive-min.css")
      (html/css "https://fonts.googleapis.com/css?family=Lato:400,400italic,700|Playfair+Display")
      (html/css "https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100")
      (html/css "/css/main.css")
      (html/meta {:charset "utf-8"})
      (html/meta {:name "viewport"
                  :content "width=device-width, initial-scale=1"})
      (html/meta {:name "description"
                  :content (or (:meta-description c) (:text (:about c)))})
      (when-let [canonical-url (:canonical-url c)]
        (html/link {:rel "canonical" :href canonical-url}))
      (html/link {:rel "icon" :href "/images/favicon.ico"})
      )
    (html/body
      {:id "body" :class "pure-g"}
      (html/header
        {:id "header" :class "pure-u-1 pure-u-lg-3-4"}
        ; hidden
        (html/a
          {:href "/"}
          (html/img {:id "logo" :alt "Passant Fox"
                     :src "/images/passant-fox.svg"}))
        (html/a
          {:href "/"}
          (html/div {:id "logo-fox-box"}))
        
        #_(html/nav
            ))
      (html/div {:class "pure-u-1" :id "header-separator"})
      (html/main
        {:id "main" :class "pure-u-1 pure-u-lg-3-4 pure-g"}
        body)
      (html/aside
        {:id "sidebar" :class "pure-u-1 pure-u-lg-1-4"}
        (html/section
          (html/h1 {:class "boxy-inline"} "About me")
          (decorate (:text (:about c)))
         ;(html/p
         ;  "Nas cumprisse dar estroinas nos industria era consintas. Braco mas madre novos susto ser. Parecia ter levarem dragoes inanime dor..."
         ;  (html/a {:href "#"} "Levarem Ostos"))
          ))
      (html/footer
        {:id "footer" :class "pure-u-1-1"}
        (let [year (-> (. Calendar getInstance) (. get Calendar/YEAR))]
          (str "© " year " - All Rights Reserved"))
        #_(html/address)
        ))))

; ; ; ; ; ; ; ; ; ; ; ;

(defn admin-document [c]
  (html/html
    (html/head
      (html/title (:title c))
      ;(html/css "http://yui.yahooapis.com/combo?pure/0.6.0/pure-min.css&pure/0.6.0/grids-responsive-min.css")
      (html/css "/css/grids-responsive-min.css")
      (html/meta {:name "viewport"
                  :content "width=device-width, initial-scale=1"})
      (html/css "/css/admin.css"))
    (html/body)
    (html/script {:type "text/javascript" :src "/js/admin/admin.js"})))

; ; ; ; ; ; ; ; ; ; ; ;

(defn asking-for-edn? [req]
  (= (request/content-type req) "application/edn"))

(def random-text
  (->> "Lorem ipsum. Papsum papikumi."
       constantly (repeatedly 20) (interpose " ") str))

; ; ; ; ; ; ; ; ; ; ; ;

(def works-resource (->ResourceServer "painting" [:tag :weight]))

(defn works-by-tag
  [tag]
  (let [query (make-query works-resource)]
    (.addSort query "weight" Query$SortDirection/DESCENDING)
    (when tag (.setFilter query (ds/equality-filter "tag" tag)))
    (launch-query works-resource query)))

(defn paintings-in-edn [tag] (pr-str-safe (works-by-tag tag)))

(defn paintings-in-html [tag]
  (let [ps (->> (works-by-tag tag) (sort-by :weight))]
    (->>
      (map #(html/img {:src (str (get-in % [:images :painting]) "=s600")}) ps)
      (interpose "\n")
      (apply str))))

; ; ; ; ; ; ; ; ; ; ; ;

(def posts-resource
  (->ResourceServer "post" [:canonical-urn :title
                            :date :description :text
                            :published]))

(defn all-posts []
  (get-all
    posts-resource #(.addSort % "date" Query$SortDirection/DESCENDING)))

(defn wide-image [src] (str src "=s1024"))

(defn post-uri [post]
  (str "/post/" (:canonical-urn post) "/" (:id post)))

(defn render-posts-page [posts first-page?]
  (when-not (empty? posts)
    (let [latest-post
          (fn [post]
            (let [uri (post-uri post)]
              (html/article
                {:id "latest-post"
                 :class "pure-u-1-1"}
                (apply str (map #(html/a {:class "tag"} %) (:tags post)))
                (html/a {:href uri} (html/h1 (:title post)))
                (html/time (:date post))
                (when-let
                  [header-image (get-in post [:images "header-image"])] 
                  (html/a
                    {:href uri}
                    (html/img
                      {:class "pure-img header-image"
                       :src (wide-image header-image)})))
                (html/div
                  {:class "content"}
                  (decorate (:description post) post))
                (html/a
                  {:href uri :class "boxy-inline"}
                  "Continue Reading")
                )))
          older-post
          (fn [post]
            (let [uri (post-uri post)]
              (html/article
                {:class "pure-u-1-1 pure-u-sm-1-2"}
                (when-let
                  [header-image (get-in post [:images "header-image"])]
                  (html/a
                    {:href uri}
                    (html/img
                      {:class "pure-img header-image"
                       :src (wide-image header-image)})))
                (apply str (map #(html/a {:class "tag"} %) (:tags post)))
                (html/a {:href uri} (html/h1 (:title post)))
                (html/div
                  {:class "content"}
                  (decorate (:description post) post))
                (html/time (:date post)))))]
      (if first-page?
        (->>
          (map older-post (rest posts))
          (cons (latest-post (first posts)))
          (apply str))
        (->>
          (map older-post posts)
          (apply str))))))

(defn posts-page [page-n]
  (let [query (make-query posts-resource)
        page-size 5]
    (.addSort query "date" Query$SortDirection/DESCENDING)
    (.setFilter query (ds/equality-filter "published" true))
    (->
      (launch-query
        posts-resource query
        {:limit page-size :offset (* page-size (- page-n 1))})
      (render-posts-page page-n))))

(defn render-single-post-page [post]
  (when post
    (html/article
      {:class "pure-u-1-1"}
      (apply str (map #(html/a {:class "tag"} %) (:tags post)))
      (html/h1 (:title post))
      (html/time (:date post))
      (when-let [header-image (get-in post [:images "header-image"])]
        (html/img
          {:class "pure-img header-image"
           :src (wide-image header-image)}))
      (html/div
        {:class "content"}
        (decorate (:text post) post))
      )))

(defn get-post [id & {show-unpublished? :show-unpublished?}]
  (when-let [post (get-by-id posts-resource id)]
    (when (or (:published post) show-unpublished?)
      post
      )))

; ; ; ; ; ; ; ; ; ; ; ;

(require '[clojure.edn :as edn])

(def statics-resource
  (->ResourceServer "static" [:embedded-edn]))

(defn get-about-static []
  (when-let [static (get-by-name statics-resource "about")]
    (-> static :embedded-edn edn/read-string)))

; ; ; ; ; ; ; ; ; ; ; ;

(defn is-admin? []
  (let [user-service (UserServiceFactory/getUserService)]
    (and (.isUserLoggedIn user-service) (.isUserAdmin user-service))))

; ; ; ; ; ; ; ; ; ; ; ;

(require '[clojure.algo.generic.functor :refer [fmap]])

(defn group-by-case-constant
  [groups]
  (->>
    groups
    (map #(if (vector? (first %)) % [%]))
    (apply concat)
    (group-by first)
    (fmap #(map rest %))
    (into ())))

(defn insert-last [x col] (concat col [x]))

(defn form-resource-routes [{:keys [uri resource-server req]}]
  `[
   ;[:get ~(str uri "/name/:name") :>>
   ; #(pr-str-safe (get-by-name resource-server %))]
    [:post ~uri
     (post-edn ~resource-server ~req)]
    [:delete (str ~uri "/:key") :>>
     #(delete-entity ~resource-server (:key %))]
    [:post (str ~uri "/images/:key") :>>
     #(post-images ~resource-server ~req (:key %))]
    [:delete (str ~uri "/images/:key") :>>
     #(delete-images ~resource-server ~req (:key %))]])

(defn transformer [m] (form-resource-routes m))

(defmacro assemble-routes
  "Formed condp has nil as default value. Formed case does not
  have default therefore will throw IllegalArgumentException if
  no matches are found."
  [{:keys [case-constant condp-constant condp-test]}
   & groups]
  {:pre [(and case-constant condp-constant condp-test (not-empty groups))]}
  (let [transformed
        (map #(cond
                (vector? %) %
                (map? %) (transformer %))
             groups)
        case-grouped (group-by-case-constant transformed)]
    `(case ~case-constant
       ~@(->>
           case-grouped
           (map
             (fn [[method result-exprs]]
               [method `(condp ~condp-test ~condp-constant
                          ~@(->>
                              result-exprs
                              (apply concat)
                              (insert-last nil)))]))
           (apply concat)
           ))))

; (pprint (macroexpand-1 '(flatten-case-condp {:condp-default :what??? :case-constant (:method req) :condp-constant (:uri req) :condp-test +} [[:get "url/url/:asdasd" :>> #(+ 1 %)] [:delete "psst" :result]] [[:get :be :something]])))

; ; ; ; ; ; ; ; ; ; ; ;

(def not-found {:status 404})
(def not-authorized not-found)

(def resource-root-uri "/resource")

(defn serve-post [id & {show-unpublished? :show-unpublished?}]
  (if-let [post (get-post id :show-unpublished? show-unpublished?)]
    (response
      (document
        {:title (str (:title post) " | Passant Fox")
         :about (get-about-static)
         :canonical-url (str "http://passantfox.com" (post-uri post))
         :meta-description (:description post)}
        (render-single-post-page post)))
    not-found))

(defn serve-front-page [page-number]
  (response
    (document {:title "Passant Fox | Elegant elegant."
               :about (get-about-static)}
              (posts-page page-number))))

(defn router [req]
  (assemble-routes
    {:case-constant (:request-method req)
     :condp-constant (:uri req)
     :condp-test match-uri}
    ;;;;;;;;;;

    ; posts for user
    [:get "/post/:any-urn/:id" :>>
     #(serve-post (:id %))]
    [:get "/preview/:id" :>>
     #(serve-post (:id %) :show-unpublished? true)]
    ;;;;;;;;;;

    ; front-page
    [:get "/"
     (serve-front-page 1)]
    [:get "/page/:number" :>>
     #(serve-front-page (:number %))]
    ;;;;;;;;;;

    [:get "/admin/:*?path"
     (response (admin-document {:title "Admin"}))]
    [:post (str resource-root-uri "/upload-url")
     (response (bs/get-blob-upload-url (request/body-string req)))]

    ;;; admin if comms start
    ;;
    ; posts
    [:get (str resource-root-uri "/posts")
     (response (pr-str-safe (all-posts)))]
    {:uri (str resource-root-uri "/posts")
     :resource-server posts-resource :req req}
    ;;;;;;;;;;

    ; statics
    [:get (str resource-root-uri "/statics")
     (response (pr-str-safe (get-all statics-resource)))]
    {:uri (str resource-root-uri "/statics")
     :resource-server statics-resource :req req}
    ;;;;;;;;;;
    ;;
    ;;; admin if comms end

    ))

(defn app [req]
  (let [response (router req)]
    (if (nil? response)
      not-found
      response
      )))
; #_(defn app [req]
;   (case (:request-method req)
;     :get
;     (condp match-uri (:uri req)
;       "/" {:body (document
;                   {:title "Passant Fox | Elegant elegant."}
;                   (posts-page 1))}
;       "/preview/:urn"
;       :>>
;       (fn [{urn :urn}]
;         (if (is-admin?)
;           (if-let [post (get-post urn :show-unpublished? true)]
;             {:body
;             (document
;               {:title (str (:title post) " | Passant Fox")}
;               (render-single-post-page post))}
;             not-found)
;           not-authorized))
;       "/post/:urn"
;       :>>
;       (fn [{urn :urn}]
;         (if-let [post (get-post urn)]
;           {:body
;           (document
;             {:title (:title post)}
;             (render-single-post-page post))}
;           not-found))
;       "/admin/:*?path" :>>
;       (fn [{path :*?path}]
;         (if (is-admin?)
;           (if (asking-for-edn? req)
;             (condp match-uri path
;               "posts" {:body (pr-str-safe (all-posts))}
;               "works/:?tag" :>> (fn [{tag :?tag}]
;                                   {:body (paintings-in-edn tag)})
;               not-found)
;             {:body (admin-document {:title "Admin"})})
;           not-authorized))
;       not-found)
;     :post
;     (condp match-uri (:uri req)
;       "/admin/upload-url" {:body (bs/get-blob-upload-url
;                                   (request/body-string req))}
;       "/admin/posts" (post-edn posts-resource req)
;       "/admin/posts/images/:key" :>>
;       #(post-images posts-resource req (:key %))
;       not-found)
;     :delete
;     (condp match-uri (:uri req)
;       "/admin/posts/:key" :>>
;       #(delete-entity posts-resource (:key %))
;       "/admin/posts/images/:key" :>>
;       #(delete-images posts-resource req (:key %))
;       not-found)
;     ))
