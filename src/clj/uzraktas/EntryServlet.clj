(ns uzraktas.EntryServlet
  (:gen-class :extends javax.servlet.http.HttpServlet)
  (:require
    [ring.util.servlet :refer [defservice]]
    [uzraktas.core :refer [app]]))

(defservice app)
