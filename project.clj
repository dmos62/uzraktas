(defproject uzraktas "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.7.0-beta2"]
                 [org.clojure/clojurescript "0.0-3269"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/algo.generic "0.1.2"]
                 [commons-fileupload/commons-fileupload "1.3.1"]
                ;[org.apache.commons/commons-io "1.3.2"]
                 [com.google.appengine/appengine-api-1.0-sdk "1.9.20"]
                 [com.google.appengine/appengine-api-labs "1.9.20"]
                 [com.google.appengine/appengine-jsr107cache "1.9.20"]
                 [ring "1.3.2"]
                 [ring/ring-ssl "0.2.1"]
                 [prismatic/om-tools "0.3.10"]
                 [org.omcljs/om "0.8.8"]
                 [markdown-clj "0.9.65"]
                 [org.clojure/tools.trace "0.7.8"]
                 [org.clojure/tools.logging "0.3.1"]
                 [fipp "0.5.2"]
                 [criterium "0.4.3"]]
  :aot :all
  :plugins [[lein-cljsbuild "1.0.5"]
            [lein-libdir "0.1.1"]]
  :libdir-path "war/WEB-INF/lib" ; `lein libdir` copies dependency jars
  :compile-path "war/WEB-INF/classes"
  :source-paths ["src/clj" "src/cljc"
                 "src/cljs/main" "src/cljs/admin"
                 "war/WEB-INF/classes"]
  :test-paths   ["test/clj" "test/cljs" "test/cljc"]
  :clean-targets ^{:protect false} [:compile-path :libdir-path "war/js"]
  ;:hooks [leiningen.cljsbuild] ; kol nedirbu su js, nereikia
  :min-lein-version "2.5.0"
  :resource-paths ["resources" ; TODO kam resources?
                   "gae-jars/appengine-local-runtime-shared.jar"
                   "gae-jars/el-api.jar"
                   "gae-jars/jsp-api.jar"
                   "gae-jars/servlet-api.jar"]

  :repl-options {:prompt 
                 (fn [ns]
                   (str "\033[1;37m"
                        ns "=>"
                        "\033[0m "))
                 :welcome 
                 (println ":welcome to the striking world of the repl...")}

  :cljsbuild
  {:builds
   {:dev
    {:source-paths
     ["src/cljc" "src/cljs/main" "src/cljs/admin" "test/cljs" "test/cljc"]
     :compiler {:main uzraktas.admin.core
                :asset-path "/js/admin/out"
                :output-to "war/js/admin/admin.js"
                :output-dir "war/js/admin/out"
                :source-map true
                :optimizations :none
                :cache-analysis true
                :pretty-print true
                }}
    :prod
    {:source-paths
     ["src/cljc" "src/cljs/main" "src/cljs/admin" "test/cljs" "test/cljc"]
     :compiler {:main uzraktas.admin.core
                :asset-path "/js/admin/out"
                :output-to "war/js/admin/admin.js"
                :optimizations :simple
                :pretty-print false
                }}}}

  :profiles
  {:dev
   {:dependencies
    [[com.google.appengine/appengine-api-stubs "1.9.20"]
     [com.google.appengine/appengine-testing "1.9.20"]]
    }}
)
