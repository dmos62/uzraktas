(ns user
  (:require 
    [clojure.tools.namespace.repl :refer (refresh)]
    [clojure.test :refer (run-tests run-all-tests)]
    [clojure.tools.trace :refer :all]
    ;[local-gae :refer [init-app-engine start-server stop-server]]
    [uzraktas.core :refer [app]]
    ))
